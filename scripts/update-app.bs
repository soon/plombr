#!/bin/bash

set -e

git pull
docker-compose up -d --build
docker-compose exec backend ./manage.py migrate
