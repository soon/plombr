import { message } from 'antd'
import forOwn from 'lodash/forOwn'

export function showResponseMessage(response: any) {
  if (response && response.message) {
    const types = {
      info: message.info,
      success: message.success,
      error: message.error,
      warning: message.warning,
      loading: message.loading
    }
    forOwn(types, (fn, key) => {
      if (response.message[key]) {
        fn(response.message[key])
      }
    })
  }
}
