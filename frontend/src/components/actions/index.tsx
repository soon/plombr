import { NextRouter } from 'next/dist/next-server/lib/router/router'

export async function applyResponseAction(response: any, router: NextRouter) {
  if (response && response.action) {
    const { action } = response
    switch (action.type) {
      case 'redirect':
        await router.push('/[...slug]', action.url)
    }
  }
}
