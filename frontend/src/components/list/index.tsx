import DynamicComponent from '@/components/core/dynamic-component'
import { ListStore } from '@/components/list/store'
import { List as AntdList } from 'antd'
import { observer } from 'mobx-react-lite'
import React from 'react'

type Props = {
  store: ListStore
}

const ListItem = observer(({ listStore, item, ...props }: { listStore: ListStore; item: any }) => {
  return <DynamicComponent {...props} kind={item.kind ?? listStore.itemKind} data={item} />
})

const List = observer(({ store, ...props }: Props) => {
  return (
    <AntdList
      dataSource={store.items}
      renderItem={(item) => (
        <AntdList.Item>
          <ListItem {...props} listStore={store} item={item} />
        </AntdList.Item>
      )}
    />
  )
})

export default List
