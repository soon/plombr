import { extendObservable } from 'mobx'

type Props = {
  itemKind?: string
  items: any[]
}

export class ListStore {
  itemKind: string
  items: any[]

  constructor({ itemKind, items }: Props) {
    extendObservable(this, {
      itemKind,
      items
    })
  }
}
