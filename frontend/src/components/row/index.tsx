import Container from '@/components/core/dynamic-component/base/container'
import { RowStore } from '@/components/row/store'
import { Row as AntdRow } from 'antd'
import { observer } from 'mobx-react-lite'
import React from 'react'
import css from './style.module.css'

type Props = {
  store: RowStore
}

const Row = observer(({ store, ...props }: Props) => {
  return (
    <AntdRow className={css.row} gutter={16}>
      <Container store={store.itemsContainer} {...props} />
    </AntdRow>
  )
})

export default Row
