import { ContainerStore, DynamicItem } from '@/components/core/dynamic-component/base/container/store'
import { extendObservable } from 'mobx'

export class RowStore {
  itemsContainer: ContainerStore

  constructor({ items }: { items: DynamicItem[] }) {
    extendObservable(this, {
      itemsContainer: new ContainerStore(items)
    })
  }
}
