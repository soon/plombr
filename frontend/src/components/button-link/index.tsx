import { Button } from 'antd'
import { ButtonProps } from 'antd/lib/button/button'
import { useRouter } from 'next/router'
import React from 'react'

type Props = ButtonProps & {
  url: string
}

export default function ButtonLink({ url, ...props }: Props) {
  const router = useRouter()
  return (
    <Button
      {...props}
      onClick={async () => {
        await router.push('/[...slug]', url)
      }}
    />
  )
}
