import { FC } from 'react'

type ComponentDescriptor = {
  component: any
  store?: any
}

const componentsRegistry = new Map<string, ComponentDescriptor>()

export function registerComponent(kind: string, component: FC<any>, store?: any) {
  const existing = componentsRegistry.get(kind)
  if (existing != null && existing.component !== component) {
    console.log(`Overwriting ${kind}`)
  }
  componentsRegistry.set(kind, {
    component,
    store
  })
}

export function getComponent(kind: string): ComponentDescriptor | undefined {
  return componentsRegistry.get(kind)
}
