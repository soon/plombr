import { getComponent } from '@/components/core/dynamic-component/registry'
import { makeAutoObservable } from 'mobx'
import isPlainObject from 'lodash/isPlainObject'

export class DynamicComponentStore {
  kind: string
  store?: any
  data?: any

  constructor(kind: string, data: any) {
    this.kind = kind
    const cd = this.getComponent()
    let finalData = data
    if (isPlainObject(data)) {
      const keys = Object.keys(data)
      if (keys.length === 1 && keys[0] === 'data') {
        finalData = data.data
      }
    }
    if (cd != null) {
      const { store } = cd
      if (store != null) {
        this.store = store && new store(finalData)
      } else {
        this.data = finalData
      }
    }
    makeAutoObservable(this)
  }

  getComponent() {
    return getComponent(this.kind)
  }

  setData(value: any) {
    if (this.store) {
      if (this.store.setData) {
        this.store.setData(value)
      } else {
        throw Error(`${typeof this.store} must define setData`)
      }
    } else {
      this.data = value
    }
  }
}
