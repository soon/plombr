import React from 'react'

type Props = {
  kind: string
  [key: string]: any
}

export default function UnknownComponent({ kind, ...props }: Props) {
  return (
    <span>
      Unknown element: {`${kind}`}, {Object.keys(props).map((x) => `${x}: ${props[x]}`)}
    </span>
  )
}
