import { getComponent } from '@/components/core/dynamic-component/registry'
import { DynamicComponentStore } from '@/components/core/dynamic-component/store'
import UnknownComponent from '@/components/core/dynamic-component/unknown-component'
import { observer } from 'mobx-react-lite'
import React from 'react'

type InternalDynamicComponent = {
  store: DynamicComponentStore
  [key: string]: any
}

export const InternalDynamicComponent = observer(({ store, ...props }: InternalDynamicComponent) => {
  const { kind, store: componentStore, data } = store
  const { component: Element } = getComponent(kind) || {}
  if (Element != null) {
    return <Element {...props} kind={kind} store={componentStore} data={data} />
  }
  return <UnknownComponent kind={kind} {...props} />
})

type Props = {
  kind: string
  store?: any
  data?: any
  [key: string]: any
}

const DynamicComponent = observer(({ kind, store, data, ...props }: Props) => {
  const { component: Element } = getComponent(kind) || {}
  if (Element != null) {
    return <Element {...props} kind={kind} store={store} data={data} />
  }
  return <UnknownComponent kind={kind} {...props} />
})

export default DynamicComponent
