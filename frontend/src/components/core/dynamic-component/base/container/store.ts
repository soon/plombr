import { DynamicComponentStore } from '@/components/core/dynamic-component/store'
import { extendObservable } from 'mobx'

export type DynamicItem = {
  kind: string
  [others: string]: any
}

export class ContainerStore {
  items: DynamicComponentStore[] = []

  constructor(items: DynamicItem[]) {
    extendObservable(this, {
      items: items.map(({ kind, ...rest }) => new DynamicComponentStore(kind, rest))
    })
  }
}
