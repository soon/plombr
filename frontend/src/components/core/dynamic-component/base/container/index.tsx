import { InternalDynamicComponent } from '@/components/core/dynamic-component'
import { ContainerStore } from '@/components/core/dynamic-component/base/container/store'
import { observer } from 'mobx-react-lite'
import React from 'react'

type Props = {
  store: ContainerStore
  [others: string]: any
}

const Container = observer(({ store, ...extraProps }: Props) => {
  return (
    <>
      {store.items.map((store, idx) => (
        <InternalDynamicComponent store={store} key={idx} {...extraProps} />
      ))}
    </>
  )
})

export default Container
