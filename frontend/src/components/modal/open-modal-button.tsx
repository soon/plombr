import { ModalStore } from '@/components/modal/store'
import { PageStore } from '@/components/pages/generic/store'
import { Button } from 'antd'
import { observer } from 'mobx-react-lite'
import React from 'react'

type Props = {
  data: {
    title: string
    modalKey: string
  }
  pageStore: PageStore
}

const OpenModalButton = observer(({ data, pageStore }: Props) => {
  const { title, modalKey } = data
  const storeKey = `modal-${modalKey}`
  const store = pageStore.getComponentStore<ModalStore>(storeKey)

  return <Button onClick={() => store?.setOpen(true)}>{title}</Button>
})

export default OpenModalButton
