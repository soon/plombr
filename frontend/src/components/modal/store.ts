import { extendObservable } from 'mobx'

type Params = {
  title: string
  body: string
  modalKey: string
}

export class ModalStore {
  title: string
  body: string
  modalKey: string
  open: boolean

  constructor({ title, body, modalKey }: Params) {
    extendObservable(this, {
      title,
      body,
      modalKey,
      open: false
    })
  }

  setOpen(value: boolean) {
    this.open = value
  }
}
