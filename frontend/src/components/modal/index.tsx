import { ModalStore } from '@/components/modal/store'
import { PageStore } from '@/components/pages/generic/store'
import { Modal as AntdModal } from 'antd'
import isString from 'lodash/isString'
import { observer } from 'mobx-react-lite'
import React, { useEffect } from 'react'

type Props = {
  store: ModalStore
  pageStore: PageStore
}

const Modal = observer(({ store, pageStore }: Props) => {
  const storeKey = `modal-${store.modalKey}`

  useEffect(() => {
    pageStore.registerComponentStore(storeKey, store)
    return () => pageStore.removeComponentStore(storeKey)
  }, [storeKey, store, pageStore])

  return (
    <AntdModal
      title={store.title}
      visible={store?.open}
      onOk={() => store?.setOpen(false)}
      onCancel={() => store?.setOpen(false)}
    >
      {isString(store.body) ? store.body : 'not implemented'}
    </AntdModal>
  )
})

export default Modal
