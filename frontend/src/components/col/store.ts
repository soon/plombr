import { ContainerStore } from '@/components/core/dynamic-component/base/container/store'
import { makeAutoObservable } from 'mobx'

type DynamicItem = {
  kind: string
  [others: string]: any
}

export class ColStore {
  itemsContainer: ContainerStore

  constructor({ items }: { items: DynamicItem[] }) {
    makeAutoObservable(this)
    this.itemsContainer = new ContainerStore(items)
  }
}
