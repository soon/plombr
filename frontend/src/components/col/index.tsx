import Container from '@/components/core/dynamic-component/base/container'
import { ColStore } from '@/components/col/store'
import { Col as AntdCol } from 'antd'
import { observer } from 'mobx-react-lite'
import React from 'react'

type Props = {
  store: ColStore
}

const Col = observer(({ store, ...props }: Props) => {
  return (
    <AntdCol flex='auto'>
      <Container store={store.itemsContainer} {...props} />
    </AntdCol>
  )
})

export default Col
