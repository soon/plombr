import { ChartsApi } from '@/api/v1/charts/charts-api'
import { ChartDto } from '@/api/v1/charts/dto'
import ReactEcharts from 'echarts-for-react'
import React, { useEffect, useState } from 'react'

type Props = {
  data: {
    dataUrl: string
  }
}

export default function PieChart({ data }: Props) {
  const { dataUrl } = data
  const [chartData, setChartData] = useState<ChartDto | undefined>(undefined)
  useEffect(() => {
    async function loadData() {
      const data = await ChartsApi.loadChartData(dataUrl)
      setChartData(data)
    }

    loadData().then()
  }, [dataUrl, setChartData])
  if (chartData === undefined) {
    return <div>loading</div>
  }
  const finalSource = [['name', 'value'], ...chartData.source]
  const { title } = chartData
  return (
    <ReactEcharts
      option={{
        title: title
          ? {
              text: title,
              left: 'center'
            }
          : undefined,
        tooltip: {
          trigger: 'item',
          formatter: '{b}: {c} ({d}%)'
        },
        legend: {
          orient: 'vertical',
          left: 'left',
          data: ['Completed', 'Open']
        },
        dataset: {
          source: finalSource
        },
        series: [
          {
            name: 'Tasks Distribution',
            type: 'pie',
            radius: '55%',
            center: ['50%', '60%']
          }
        ]
      }}
    />
  )
}
