import React from 'react'

type Props = {
  data: string
}

export default function Text({ data }: Props) {
  if (data == null) {
    return null
  }
  return <>{String(data)}</>
}
