import FormField from '@/components/form/fields'
import { FormStore } from '@/components/form/store'
import { Button, Space } from 'antd'
import { observer } from 'mobx-react-lite'
import { useRouter } from 'next/router'
import React from 'react'

type Props = {
  store: FormStore
}

const Form = observer(({ store }: Props) => {
  const { fields } = store
  const router = useRouter()

  return (
    <form>
      <Space style={{ width: '100%' }} direction='vertical'>
        {fields.map((x, idx) => (
          <FormField store={x} key={`${idx}-${x.name}`} formStore={store} />
        ))}
        <Button type='primary' onClick={() => store.submit(router)}>
          Submit
        </Button>
      </Space>
    </form>
  )
})

export default Form
