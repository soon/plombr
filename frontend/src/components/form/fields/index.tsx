import { InternalDynamicComponent } from '@/components/core/dynamic-component'
import { Labeled } from '@/components/form/fields/labeled'
import { FormFieldStore } from '@/components/form/fields/store'
import { observer } from 'mobx-react-lite'
import React from 'react'

type Props = {
  store: FormFieldStore
  [others: string]: any
}

const FormField = observer(({ store, ...props }: Props) => {
  return (
    <Labeled store={store}>
      <InternalDynamicComponent store={store.component} fieldStore={store} {...props} />
    </Labeled>
  )
})

export default FormField
