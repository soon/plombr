import { FormFieldStore } from '@/components/form/fields/store'
import { Input } from 'antd'
import { action } from 'mobx'
import { observer } from 'mobx-react-lite'
import React, { ChangeEvent } from 'react'

type Props = {
  fieldStore: FormFieldStore
}

const TextInput = observer(({ fieldStore }: Props) => {
  return (
    <Input value={fieldStore.value} onChange={action((e: ChangeEvent<any>) => fieldStore.setValue(e.target.value))} />
  )
})

export default TextInput
