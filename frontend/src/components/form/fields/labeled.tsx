import { FormFieldStore } from '@/components/form/fields/store'
import { observer } from 'mobx-react-lite'
import React, { PropsWithChildren } from 'react'
import css from './style.module.css'

export type RawProps = {
  store: FormFieldStore
}

type Props = PropsWithChildren<RawProps>

export const Labeled = observer(({ store, children }: Props) => {
  const { label, required, helpText, errors } = store
  return (
    <div>
      {label && (
        <label className={css.label}>
          {required ? <span className={css.required}>*</span> : null} {label}:
        </label>
      )}
      {children}
      <div>
        {helpText && errors.length === 0 && <div className={css['help-text']}>{helpText}</div>}
        {errors.map((error, idx) => (
          <div className={`${css['help-text']} ${css.error}`} key={`${idx}-${error}`}>
            {error}
          </div>
        ))}
      </div>
    </div>
  )
})
