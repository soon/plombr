import { DynamicComponentStore } from '@/components/core/dynamic-component/store'
import { FormStore } from '@/components/form/store'
import { extendObservable } from 'mobx'

type Props = {
  kind: string
  name: string
  label?: string
  helpText?: string
  required: boolean
  readonly: boolean
}

export class FormFieldStore {
  form: FormStore
  name: string
  label?: string
  helpText?: string
  required: boolean
  readonly: boolean
  component: DynamicComponentStore

  constructor(form: FormStore, { kind, ...data }: Props) {
    const { name, label, helpText, required, readonly } = data
    extendObservable(this, {
      form,
      name,
      label,
      helpText,
      required: required ?? false,
      readonly: readonly ?? false,
      component: new DynamicComponentStore(kind, data)
    })
  }

  get errors() {
    return this.form.getFieldErrors(this.name)
  }

  get value() {
    return this.form.getValue(this.name)
  }

  setValue(value: any) {
    this.form.setValue(this.name, value)
  }
}
