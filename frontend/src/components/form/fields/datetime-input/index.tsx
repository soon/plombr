import { FormFieldStore } from '@/components/form/fields/store'
import { DatePicker } from 'antd'
import { action } from 'mobx'
import { observer } from 'mobx-react-lite'
import moment from 'moment'
import React from 'react'

type Props = {
  fieldStore: FormFieldStore
}

const DateTimeInput = observer(({ fieldStore }: Props) => {
  return (
    <DatePicker
      showTime
      value={fieldStore.value && moment(fieldStore.value)}
      onChange={action((_: any, valueString: string) => fieldStore.setValue(valueString))}
    />
  )
})

export default DateTimeInput
