import { FormFieldStore } from '@/components/form/fields/store'
import { FormStore } from '@/components/form/store'
import { Input } from 'antd'
import { action } from 'mobx'
import { observer } from 'mobx-react-lite'
import React, { ChangeEvent } from 'react'

type Props = {
  formStore: FormStore
  fieldStore: FormFieldStore
}

const { TextArea } = Input

const TextAreaInput = observer(({ fieldStore }: Props) => {
  return (
    <TextArea
      value={fieldStore.value}
      onChange={action((e: ChangeEvent<any>) => fieldStore.setValue(e.target.value))}
      readOnly={fieldStore.readonly}
      disabled={fieldStore.readonly}
      allowClear
      autoSize={{ minRows: 6, maxRows: 12 }}
    />
  )
})

export default TextAreaInput
