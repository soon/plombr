import { FormsApi } from '@/api/v1/forms/forms-api'
import { applyResponseAction } from '@/components/actions'
import { FormFieldStore } from '@/components/form/fields/store'
import { showResponseMessage } from '@/components/messages'
import isArray from 'lodash/isArray'
import { extendObservable, runInAction } from 'mobx'
import { NextRouter } from 'next/dist/next-server/lib/router/router'

type Props = {
  submitUrl: string
  submitMethod: string
  object?: any
  items?: any
}

export class FormStore {
  submitUrl: string
  submitMethod: string
  object?: any
  errors?: any
  fields: FormFieldStore[]
  loading: boolean

  constructor({ submitUrl, submitMethod, object, items }: Props) {
    extendObservable(this, {
      submitUrl,
      submitMethod,
      object: object || {},
      fields: items.map((x: any) => new FormFieldStore(this, x)),
      loading: false,
      errors: null
    })
  }

  getFieldErrors(name: string): string[] {
    const errors = (this.errors ?? {})[name] ?? []
    if (!isArray(errors)) {
      return [errors]
    }
    return errors
  }

  getValue(name: string) {
    return this.object[name]
  }

  setValue(name: string, value: any) {
    this.object[name] = value
  }

  async submit(router: NextRouter) {
    if (this.loading) {
      return
    }
    this.loading = true
    this.errors = null
    try {
      const results = await FormsApi.submit(this.submitUrl, this.submitMethod, this.object)
      showResponseMessage(results)
      await applyResponseAction(results, router)
    } catch (e) {
      if (e.isAxiosError) {
        const { errors } = e.response.data
        runInAction(() => {
          this.setErrors(errors)
        })
      } else {
        throw e
      }
    } finally {
      runInAction(() => {
        this.loading = false
      })
    }
  }

  setErrors(errors: any) {
    const { form } = errors
    if (form == null) {
      return
    }
    this.errors = form
  }
}
