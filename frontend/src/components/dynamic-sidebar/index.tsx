import { applyAction } from '@/actions'
import DynamicIcon from '@/components/dynamic-icon'
import { Menu } from 'antd'
import maxBy from 'lodash/maxBy'
import { useRouter } from 'next/router'
import React, { useState } from 'react'

const { SubMenu } = Menu

type MenuItem = {
  title: string
  icon?: string
  url?: string
  items?: MenuItem[]
  action?: string
}

type Props = {
  items: MenuItem[]
  className?: string
}

const ActionPrefix = '_action-'

function createItemKey(key: string, url: string, action: string | null) {
  if (action != null) {
    return `${key}:${ActionPrefix}${action}`
  }
  return `${key}:${url}`
}

function getActionFromItemKey(key: string): string | null {
  const sepIdx = key.indexOf(':')
  const name = key.substring(sepIdx + 1)
  if (name.startsWith(ActionPrefix)) {
    return name.substring(ActionPrefix.length)
  }
  return null
}

function getUrlFromItemKey(key: string) {
  const sepIdx = key.indexOf(':')
  return key.substring(sepIdx + 1)
}

function createMenuItem(keyPrefix: string, { url, items, title, icon, action }: MenuItem) {
  const itemIcon = icon ? <DynamicIcon name={icon} /> : undefined
  if (items) {
    return (
      <SubMenu key={keyPrefix} title={title} icon={itemIcon}>
        {items.map((x, idx) => createMenuItem(`${keyPrefix}-${idx}`, x))}
      </SubMenu>
    )
  }
  const key = createItemKey(keyPrefix, url ?? '', action ?? null)
  return (
    <Menu.Item key={key} icon={itemIcon}>
      {title}
    </Menu.Item>
  )
}

function collectSelectedMenuItems(
  asPath: string,
  keyPrefix: string,
  item: MenuItem,
  onCollect: (key: string, item: MenuItem) => void
) {
  const { items, url, action } = item
  if (items) {
    items.forEach((x, idx) => collectSelectedMenuItems(asPath, `${keyPrefix}-${idx}`, x, onCollect))
  } else if (url && asPath.startsWith(url)) {
    const key = createItemKey(keyPrefix, url ?? '', action ?? null)
    onCollect(key, item)
  }
}

function getSelectedOptions(asPath: string, items: MenuItem[]): string[] {
  const selectedOptions: { key: string; urlLen: number }[] = []
  const collectSelected = (key: string, selectedItem: MenuItem) =>
    selectedOptions.push({
      key,
      urlLen: (selectedItem.url ?? '').length
    })
  items.forEach((x, idx) => collectSelectedMenuItems(asPath, String(idx), x, collectSelected))
  const bestOption = maxBy(selectedOptions, 'urlLen')
  return bestOption == null ? [] : [bestOption.key]
}

function getParentItemsKeys(key: string): string[] {
  const prefix = key.split(':')[0]
  const res = []
  let sepPos = prefix.indexOf('-')
  while (sepPos > 0) {
    res.push(prefix.substring(0, sepPos))
    sepPos = prefix.indexOf('-', sepPos + 1)
  }
  return res
}

export default function DynamicSidebar({ items, className }: Props) {
  const router = useRouter()
  const [openKeys, setOpenKeys] = useState<string[]>([])
  const [userChangedOpenKeys, setUserChangedOpenKeys] = useState(false)
  if (router == null) {
    return null
  }
  const { asPath } = router
  const selectedKeys = getSelectedOptions(asPath, items)

  return (
    <Menu
      mode='inline'
      onClick={async ({ key }) => {
        const sk = String(key)
        const action = getActionFromItemKey(sk)
        if (action != null) {
          applyAction(action)
        } else {
          const url = getUrlFromItemKey(String(key))
          await router.push('/[...slug]', url)
        }
      }}
      selectedKeys={selectedKeys}
      openKeys={userChangedOpenKeys ? openKeys : selectedKeys.flatMap(getParentItemsKeys)}
      onOpenChange={(x) => {
        // @ts-ignore
        setOpenKeys(x)
        setUserChangedOpenKeys(true)
      }}
      className={className}
    >
      {items.map((x, idx) => createMenuItem(String(idx), x))}
    </Menu>
  )
}
