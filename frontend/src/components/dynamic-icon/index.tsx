import { IconsRegistry } from '@/components/dynamic-icon/registry'
import React from 'react'

type Props = {
  name: string
}

export default function DynamicIcon({ name }: Props) {
  const Component = IconsRegistry[name]
  if (Component == null) {
    return <span>unknown icon name {String(name)}</span>
  }
  return <Component />
}
