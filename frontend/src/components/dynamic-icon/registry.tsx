import {
  BarChartOutlined,
  ContactsOutlined,
  ContainerOutlined,
  DeploymentUnitOutlined,
  KeyOutlined,
  LogoutOutlined,
  ShoppingCartOutlined,
  TableOutlined,
  VideoCameraOutlined
} from '@ant-design/icons/lib'
import { FC } from 'react'

type ComponentType = FC<any>

export const IconsRegistry: { [key: string]: ComponentType } = {
  'table-outlined': TableOutlined,
  'bar-chart-outlined': BarChartOutlined,
  'deployment-unit-outlined': DeploymentUnitOutlined,
  'container-outlined': ContainerOutlined,
  'video-camera-outlined': VideoCameraOutlined,
  'shopping-cart-outlined': ShoppingCartOutlined,
  'key-outlined': KeyOutlined,
  'contacts-outlined': ContactsOutlined,
  'logout-outlined': LogoutOutlined
}
