import { TableRow } from '@/components/table/store'
import { DeleteFilled } from '@ant-design/icons'
import { Button, Popconfirm } from 'antd'
import { observer } from 'mobx-react-lite'
import React from 'react'

type Props = {
  rowStore: TableRow
}

const DeleteCell = observer(({ rowStore }: Props) => {
  return (
    <Popconfirm
      title='Confirm object deletion?'
      onConfirm={() => rowStore.applyDeleteAction()}
      okText='Yes'
      cancelText='No'
      placement='topLeft'
    >
      <Button icon={<DeleteFilled />} danger type='primary' />
    </Popconfirm>
  )
})

export default DeleteCell
