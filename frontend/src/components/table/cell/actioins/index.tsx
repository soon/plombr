import { Button, Space } from 'antd'
import { observer } from 'mobx-react-lite'
import Link from 'next/link'
import React from 'react'

type RowActionProps = {
  kind: string
  onApply: any
  [key: string]: any
}

function RowAction({ kind, onApply, ...props }: RowActionProps) {
  if (kind === 'link') {
    const { text, url } = props
    return (
      <Link href='/[...slug]' as={url}>
        <a>{text}</a>
      </Link>
    )
  }
  if (kind === 'action') {
    const { text, tableActionKey } = props
    return (
      <Button type='primary' onClick={() => onApply(tableActionKey)}>
        {text}
      </Button>
    )
  }
  return null
}

const ActionsCell = observer(({ data, rowStore }: any) => {
  return (
    <Space>
      {(data ?? []).map((x: RowActionProps, idx: number) => (
        <RowAction key={`${idx}-${x.kind}`} {...x} onApply={(name: string) => rowStore.applyAction(name)} />
      ))}
    </Space>
  )
})

export default ActionsCell
