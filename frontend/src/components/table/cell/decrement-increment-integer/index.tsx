import { TableColumn, TableRow } from '@/components/table/store'
import { Input } from 'antd'
import { observer } from 'mobx-react-lite'
import React from 'react'
import css from './style.module.css'

type Props = {
  rowStore: TableRow
  columnStore: TableColumn
  data: number
}

const DecrementIncrementIntegerCell = observer(({ rowStore, columnStore, data }: Props) => {
  return (
    <Input
      className={css.widget}
      value={data}
      addonBefore={
        <a
          className={css.button}
          onClick={async () => {
            rowStore.setValue(columnStore.name, data - 1)
            await rowStore.saveValue(columnStore.name)
          }}
        >
          -
        </a>
      }
      addonAfter={
        <a
          className={css.button}
          onClick={async () => {
            rowStore.setValue(columnStore.name, data + 1)
            await rowStore.saveValue(columnStore.name)
          }}
        >
          +
        </a>
      }
    />
  )
})

export default DecrementIncrementIntegerCell
