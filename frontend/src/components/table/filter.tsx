import { TableColumn } from '@/components/table/store'
import { FilterOutlined, SearchOutlined } from '@ant-design/icons'
import { Button, Checkbox, Input, Space } from 'antd'
import { ColumnProps } from 'antd/lib/table'
import React from 'react'

type FilterProps = Pick<ColumnProps<any>, 'filterDropdown' | 'filterIcon' | 'onFilterDropdownVisibleChange'>

export function makeFilterProps(col: TableColumn): FilterProps {
  const { filters } = col
  if (filters.length === 0 && !col.searchable) {
    return {}
  }
  const FilterIcon = filters.length === 0 ? SearchOutlined : FilterOutlined

  let searchInput: Input | null
  return {
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Space direction='vertical'>
          <Input
            ref={(node) => {
              searchInput = node
            }}
            placeholder={`Search by ${col.label}`}
            value={selectedKeys[0]}
            onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
            onPressEnter={async () => {
              confirm()
              col.setSearch(selectedKeys[0] as string)
              await col.fetchTableData()
            }}
            style={{ width: 188, display: 'block' }}
          />
          <Space>
            <Button
              type='primary'
              onClick={async () => {
                confirm()
                col.setSearch(selectedKeys[0] as string)
                await col.fetchTableData()
              }}
              icon={<SearchOutlined />}
              size='small'
              style={{ width: 90 }}
            >
              Search
            </Button>
            <Button
              onClick={async () => {
                if (clearFilters != null) {
                  clearFilters()
                }
                col.setSearch('')
                await col.fetchTableData()
              }}
              size='small'
              style={{ width: 90 }}
            >
              Reset
            </Button>
          </Space>
          {filters.map((filter) => (
            <Checkbox
              key={name}
              onChange={async (e) => {
                filter.setSelected(e.target.checked)
                await col.fetchTableData()
              }}
            >
              {filter.label}
            </Checkbox>
          ))}
        </Space>
      </div>
    ),
    filterIcon: (filtered) => <FilterIcon style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput?.select())
      }
    }
  }
}
