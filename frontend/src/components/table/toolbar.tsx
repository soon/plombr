import ButtonLink from '@/components/button-link'
import TableActions from '@/components/table/actions'
import { TableStore } from '@/components/table/store'
import { PlusOutlined } from '@ant-design/icons'
import { Divider, Input, Space } from 'antd'
import { observer } from 'mobx-react-lite'
import React from 'react'

import css from './style.module.css'

const { Search } = Input

type Props = {
  tableStore: TableStore
}

export const Toolbar = observer(({ tableStore }: Props) => {
  const { actions, addNewUrl } = tableStore
  const hasActions = actions.length > 0

  return (
    <div className={css.toolbar}>
      <Search
        placeholder='Search'
        onSearch={async (x) => {
          tableStore.setSearch(x)
          await tableStore.fetchTableData()
        }}
        style={{ width: 300 }}
      />
      <div className={css.spacer} />
      <Space>
        {hasActions && <TableActions tableStore={tableStore} />}
        {hasActions && addNewUrl && <Divider type='vertical' />}
        {addNewUrl && (
          <ButtonLink url={addNewUrl} icon={<PlusOutlined />} type='primary'>
            Add New
          </ButtonLink>
        )}
      </Space>
    </div>
  )
})
