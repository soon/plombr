import { TablesApi } from '@/api/v1/tables/tables-api'
import { DynamicComponentStore } from '@/components/core/dynamic-component/store'
import { showResponseMessage } from '@/components/messages'
import { extendObservable, makeAutoObservable, observable, runInAction } from 'mobx'

export class TableRow {
  tableStore: TableStore
  id: any
  cells: Map<string, DynamicComponentStore>
  customProps: Map<string, any>

  constructor(tableStore: TableStore, data: any, columns: TableColumn[]) {
    this.tableStore = tableStore
    this.id = data.id
    this.cells = new Map<string, DynamicComponentStore>()
    columns.forEach(({ type, name }) => {
      this.cells.set(name, new DynamicComponentStore(type, data[name]))
    })
    this.customProps = new Map<string, any>()
    makeAutoObservable(this)
  }

  async applyAction(name: string) {
    return this.tableStore.applyAction(name, [this.id])
  }

  async applyDeleteAction() {
    return this.applyAction('delete')
  }

  setValue(name: string, value: any) {
    const cellStore = this.cells.get(name)
    if (cellStore != null) {
      cellStore.setData(value)
    }
  }

  async saveValue(name: string) {
    const cellStore = this.cells.get(name)
    if (cellStore != null) {
      return this.tableStore.patchObject(this.id, { [name]: cellStore.data })
    }
  }

  setCustomProp(name: string, value: any) {
    this.customProps.set(name, value)
  }

  getCustomProp(name: string) {
    return this.customProps.get(name)
  }
}

export class TableColumnFilter {
  name: string
  label: string
  selected: boolean

  constructor({ name, label }: any) {
    this.selected = false
    extendObservable(this, { name, label })
  }

  setSelected(selected: boolean) {
    this.selected = selected
  }
}

export class TableColumn {
  tableStore: TableStore
  type: string
  name: string
  label: string
  sortable: boolean
  filters: TableColumnFilter[]
  searchable: boolean
  widget: any
  search = ''

  constructor(tableStore: TableStore, { type, name, label, sortable, filters, searchable, widget }: any) {
    this.tableStore = tableStore
    this.filters = (filters ?? []).map((x: any) => new TableColumnFilter(x))
    extendObservable(this, { type, name, label, sortable, searchable, widget })
  }

  setSearch(value?: string) {
    if (value == null) {
      value = ''
    }
    this.search = value
  }

  async fetchTableData() {
    return this.tableStore.fetchTableData()
  }
}

export class TableAction {
  name: string
  label: string

  constructor({ name, label }: any) {
    extendObservable(this, {
      name,
      label
    })
  }
}

export class TableStore {
  dataUrl: string
  actionUrl: string
  addNewUrl: string
  loading: boolean
  retryDataFetching = false
  data: TableRow[]
  columns: TableColumn[]
  actions: TableAction[]

  orderBy: string | null
  search: string
  selectedAction: string | undefined
  applyingAction: boolean
  selectedIds: any[]

  constructor({ dataUrl, actionUrl, addNewUrl, columns, actions }: any) {
    extendObservable(this, {
      dataUrl,
      actionUrl,
      addNewUrl,
      orderBy: null,
      columns: columns.map((x: any) => new TableColumn(this, x)),
      actions: actions.map((x: any) => new TableAction(x)),
      data: [],
      loading: false,
      search: '',
      selectedAction: null,
      applyingAction: false,
      selectedIds: []
    })
  }

  private async doFetchTableData() {
    const params: any = {}
    this.columns.forEach((col) => {
      if (col.search.length !== 0) {
        params[`${col.name}__icontains`] = col.search
      }
      col.filters
        .filter((x) => x.selected)
        .forEach(({ name }) => {
          params[`${col.name}__${name}`] = true
        })
    })
    if (this.orderBy != null) {
      params.o = this.orderBy
    }
    if (this.search.length > 0) {
      params.q = this.search
    }
    return await TablesApi.loadTableData(this.dataUrl, params)
  }

  private async doFetchTableDataWhileNeeded() {
    let data = await this.doFetchTableData()
    while (this.retryDataFetching) {
      runInAction(() => {
        this.retryDataFetching = false
      })
      data = await this.doFetchTableData()
    }
    return data
  }

  private async doFetchAndSetDataWhileNeeded() {
    const data = await this.doFetchTableDataWhileNeeded()
    runInAction(() => {
      this.data = data.rows.map((x) => new TableRow(this, x, this.columns))
    })
  }

  async fetchTableData() {
    if (this.loading) {
      this.retryDataFetching = true
      return
    }
    runInAction(() => {
      this.loading = true
    })
    await this.doFetchAndSetDataWhileNeeded()
    runInAction(() => {
      this.loading = false
    })
  }

  async doApplyAction(name: string, ids: string[] | number[]) {
    return await TablesApi.applyTableAction(this.actionUrl, name, ids)
  }

  async applyAction(name: string, ids: string[] | number[]) {
    if (this.loading) {
      return
    }
    runInAction(() => {
      this.loading = true
    })
    const result = await this.doApplyAction(name, ids)
    showResponseMessage(result)
    await this.doFetchAndSetDataWhileNeeded()
    runInAction(() => {
      this.loading = false
    })
  }

  async patchObject(objId: number | string, data: object) {
    if (this.loading) {
      return
    }
    runInAction(() => {
      this.loading = true
    })
    const result = await TablesApi.patchTableObject(this.dataUrl, { objId }, data)
    showResponseMessage(result)
    await this.doFetchAndSetDataWhileNeeded()
    runInAction(() => {
      this.loading = false
    })
  }

  async applySelectedAction() {
    if (this.loading || this.applyingAction || this.selectedAction == null) {
      return
    }
    runInAction(() => {
      this.loading = true
      this.applyingAction = true
    })
    const result = await this.doApplyAction(this.selectedAction, this.selectedIds)
    showResponseMessage(result)
    runInAction(() => {
      this.applyingAction = false
    })
    await this.doFetchAndSetDataWhileNeeded()
    runInAction(() => {
      this.loading = false
    })
  }

  setOrderBy(value: string | null) {
    this.orderBy = value
  }

  setSearch(value: string) {
    this.search = value
  }

  setSelectedAction(value: string) {
    this.selectedAction = value
  }

  setSelectedIds(value: any[]) {
    this.selectedIds = observable.array(value)
  }
}
