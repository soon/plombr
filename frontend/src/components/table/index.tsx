import { InternalDynamicComponent } from '@/components/core/dynamic-component'
import { makeFilterProps } from '@/components/table/filter'
import { TableRow, TableStore } from '@/components/table/store'
import { Toolbar } from '@/components/table/toolbar'
import { Space, Table as AntdTable } from 'antd'
import { SorterResult, TablePaginationConfig } from 'antd/lib/table/interface'
import { observer } from 'mobx-react-lite'
import React, { useEffect } from 'react'

const { Column } = AntdTable

type Props = {
  store: TableStore
}

const Table = observer(({ store }: Props) => {
  const { columns, data, loading } = store
  useEffect(() => {
    store.fetchTableData().then()
  }, [store])

  return (
    <Space direction='vertical' style={{ width: '100%' }}>
      <Toolbar tableStore={store} />
      <AntdTable
        rowKey='id'
        dataSource={data}
        loading={loading}
        onChange={async (_: TablePaginationConfig, __: any, sorter: SorterResult<any>) => {
          if (sorter?.order == null) {
            store.setOrderBy(null)
          } else {
            store.setOrderBy(`${sorter.order === 'descend' ? '-' : ''}${sorter.field}`)
          }
          await store.fetchTableData()
        }}
        rowSelection={{
          selectedRowKeys: store.selectedIds,
          onChange: (selectedKeys) => {
            // @ts-ignore
            const keys: string[] | number[] = selectedKeys
            store.setSelectedIds(keys)
          }
        }}
      >
        {columns.map((col, idx) => (
          <Column
            title={col.label}
            dataIndex={col.name}
            key={`${idx}-${col.label}`}
            sorter={Boolean(col.sortable)}
            align={col.type === 'delete' ? 'center' : undefined}
            render={(_: any, record: TableRow) => {
              const cell = record.cells.get(col.name)
              if (cell != null) {
                return <InternalDynamicComponent store={cell} rowStore={record} columnStore={col} />
              }
              return null
            }}
            {...makeFilterProps(col)}
          />
        ))}
      </AntdTable>
    </Space>
  )
})

export default Table
