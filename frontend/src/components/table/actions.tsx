import { TableStore } from '@/components/table/store'
import { Button, Select, Space } from 'antd'
import { observer } from 'mobx-react-lite'
import React from 'react'

type Props = {
  tableStore: TableStore
}

const TableActions = observer(({ tableStore }: Props) => {
  const { actions, selectedAction } = tableStore
  return (
    <Space>
      <Select
        style={{ minWidth: 200 }}
        placeholder='Select action'
        value={selectedAction}
        onChange={(x) => tableStore.setSelectedAction(x)}
      >
        {actions.map((x) => (
          <Select.Option key={x.name} value={x.name}>
            {x.label}
          </Select.Option>
        ))}
      </Select>
      <Button
        disabled={!selectedAction || tableStore.loading || tableStore.applyingAction}
        onClick={async () => {
          await tableStore.applySelectedAction()
        }}
        loading={tableStore.applyingAction}
      >
        Apply
      </Button>
    </Space>
  )
})

export default TableActions
