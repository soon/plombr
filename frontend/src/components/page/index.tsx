import Container from '@/components/core/dynamic-component/base/container'
import { PageStore } from '@/components/page/store'
import { observer } from 'mobx-react-lite'
import React from 'react'

type Props = {
  store: PageStore
}

const Page = observer(({ store }: Props) => {
  return <Container store={store.itemsContainer} pageStore={store} />
})

export default Page
