import { ContainerStore } from '@/components/core/dynamic-component/base/container/store'
import { makeAutoObservable } from 'mobx'

type DynamicItem = {
  kind: string
  [others: string]: any
}

export class PageStore {
  itemsContainer: ContainerStore
  components = new Map<string, any>()

  constructor({ items }: { items: DynamicItem[] }) {
    makeAutoObservable(this)
    this.itemsContainer = new ContainerStore(items)
  }

  registerComponentStore(name, store) {
    this.components.set(name, store)
  }

  removeComponentStore(name) {
    this.components.delete(name)
  }

  getComponentStore<T>(name): T | undefined {
    return this.components.get(name)
  }
}
