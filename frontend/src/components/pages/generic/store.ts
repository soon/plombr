import { ContainerStore, DynamicItem } from '@/components/core/dynamic-component/base/container/store'
import { extendObservable } from 'mobx'

export class PageStore {
  title?: string
  items: ContainerStore

  userStores: Map<string, any>

  constructor({ title, items }: { title?: string; items?: DynamicItem[] }) {
    extendObservable(this, {
      title,
      items: new ContainerStore(items ?? []),
      userStores: new Map<string, any>()
    })
  }

  setItems(items: DynamicItem[]) {
    this.items = new ContainerStore(items)
  }

  public getComponentStore<T>(itemKey: string): T | null {
    return this.userStores.get(itemKey)
  }

  public registerComponentStore<T>(storeKey: string, store: T) {
    this.userStores.set(storeKey, store)
  }

  public removeComponentStore(storeKey: string) {
    this.userStores.delete(storeKey)
  }
}
