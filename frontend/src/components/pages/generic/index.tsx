import Container from '@/components/core/dynamic-component/base/container'
import { DynamicItem } from '@/components/core/dynamic-component/base/container/store'
import { PageStore } from '@/components/pages/generic/store'
import { observer } from 'mobx-react-lite'
import React, { useEffect } from 'react'

type Props = {
  items: DynamicItem[]
}

const pageStore = new PageStore({})

const GenericPage = observer(({ items }: Props) => {
  useEffect(() => {
    pageStore.setItems(items)
  }, [items])

  return <Container store={pageStore.items} pageStore={pageStore} />
})

export default GenericPage
