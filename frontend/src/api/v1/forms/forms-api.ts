import { BaseApi } from '@/api/base/base'

class FormsApiImpl extends BaseApi {
  constructor() {
    super('/api/v1/forms/')
  }

  async submit(url: string, method: string, data: object): Promise<any> {
    return (
      await this.axios.request({
        url,
        method: <'post' | 'put'>method,
        data
      })
    ).data
  }
}

export const FormsApi = new FormsApiImpl()
