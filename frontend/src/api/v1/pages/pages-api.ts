import { BaseApi } from '@/api/base/base'

type Page = {
  page: PageDescription
}

type PageDescription = {
  type: string
  title: string
}

class PagesApiImpl extends BaseApi {
  constructor() {
    super('/api/v1/pages/')
  }

  async resolve(url: string): Promise<Page> {
    return this.getData('resolve', { params: { url } })
  }
}

export const PagesApi = new PagesApiImpl()
