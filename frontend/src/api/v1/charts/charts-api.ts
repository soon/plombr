import { BaseApi } from '@/api/base/base'
import { ChartDto } from '@/api/v1/charts/dto'

class ChartsApiImpl extends BaseApi {
  constructor() {
    super('/api/v1/charts/')
  }

  async loadChartData(url: string): Promise<ChartDto> {
    return (await this.axios.get(url)).data
  }
}

export const ChartsApi = new ChartsApiImpl()
