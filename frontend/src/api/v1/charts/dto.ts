export type ChartDto = {
  title?: string
  source: [string, any][]
}
