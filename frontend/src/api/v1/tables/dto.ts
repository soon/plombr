export type TableConfigDto = {
  actionUrl: string
  addNewUrl: string
  dataUrl: string
  columns: TableColumnDto[]
  actions: TableActionDto[]
}

export type TableColumnDto = {
  type: string
  name: string
  label: string
  sortable: boolean
  filters: any
  searchable: boolean
  widget: any
}

export type TableActionDto = {
  name: string
  label: string
}

export type TableDataDto<T> = {
  rows: T[]
}
