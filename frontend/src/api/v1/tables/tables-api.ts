import { BaseApi } from '@/api/base/base'
import { TableDataDto } from '@/api/v1/tables/dto'

class TablesApiImpl extends BaseApi {
  constructor() {
    super('/api/v1/tables/')
  }

  async loadTableData<T = object>(url: string, params?: object): Promise<TableDataDto<T>> {
    return (
      await this.axios.get(url, {
        params
      })
    ).data
  }

  async patchTableObject<T = object>(url: string, params?: object, data?: object): Promise<TableDataDto<T>> {
    return (
      await this.axios.patch(url, data, {
        params
      })
    ).data
  }

  async applyTableAction(url: string, action: string, ids: string[] | number[]): Promise<any> {
    return (
      await this.axios.post(url, {
        action: action,
        ids: ids
      })
    ).data
  }
}

export const TablesApi = new TablesApiImpl()
