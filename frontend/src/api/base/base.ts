import Axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios'

export const axios = Axios.create()

export class BaseApi {
  protected axios: AxiosInstance
  private readonly prefix: string

  constructor(prefix: string) {
    this.axios = axios
    this.axios.defaults.xsrfCookieName = 'csrftoken'
    this.axios.defaults.xsrfHeaderName = 'X-CSRFToken'
    this.prefix = prefix
  }

  protected get<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return this.axios.get(`${this.prefix}${url}`, config)
  }

  protected async getData<T = any>(url: string, config?: AxiosRequestConfig): Promise<T> {
    return (await this.get<T>(url, config)).data
  }
}
