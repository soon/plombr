import { axios } from '@/api/base/base'

export { PagesApi } from '@/api/v1/pages/pages-api'

export function getAxios() {
  return axios
}
