type Action = {
  (data?: any): any | Promise<any> | null
}

const registry = new Map<string, Action>()

export function registerAction(key: string, action: Action) {
  const existing = registry.get(key)
  if (existing != null && existing !== action) {
    console.log(`Overwriting ${key}`)
  }
  registry.set(key, action)
}

export function applyAction<T = any>(key: string, data?: any): T | Promise<T> | null {
  const action = registry.get(key)
  if (action != null) {
    return action(data)
  }
  return null
}

export async function applyActions(actions: { name: string; data: any }[]) {
  for (const action of actions) {
    const result = applyAction(action.name, action.data)
    if (result != null) {
      await Promise.resolve(result)
    }
  }
}
