import { getToken } from '@/auth'
import Layout from '@/components/layout'
import PageTitle from '@/components/page-title'
import { Spin } from 'antd'
import Router from 'next/router'
import React, { useEffect, useState } from 'react'

export default function Home() {
  const [loaded, setLoaded] = useState(false)
  useEffect(() => {
    const checkToken = async () => {
      try {
        const token = await getToken()
        if (token == null) {
          await Router.push('/login')
        }
      } finally {
        setLoaded(true)
      }
    }

    if (typeof window !== 'undefined') {
      checkToken().then()
    }
  }, [setLoaded])

  if (!loaded) {
    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          minHeight: '100vh',
          width: '100vw',
          flexDirection: 'column'
        }}
      >
        <Spin />
      </div>
    )
  }

  return (
    <>
      <PageTitle title='Home' />
      <Layout title='Home'>
        <h1>Hey there!</h1>
        <p>Start exploring by clicking on menu items</p>
      </Layout>
    </>
  )
}
