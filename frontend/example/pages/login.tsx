import LoginForm from '@/components/forms/login'
import PageTitle from '@/components/page-title'
import * as React from 'react'

export default function LoginPage() {
  return (
    <>
      <PageTitle title='Login' />
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          minHeight: '100vh',
          width: '100vw',
          flexDirection: 'column'
        }}
      >
        <h1>Plombr Demo</h1>
        <LoginForm />
      </div>
    </>
  )
}
