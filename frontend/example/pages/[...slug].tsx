import Layout from '@/components/layout'
import PageTitle from '@/components/page-title'
import { message } from 'antd'
import { NextPage } from 'next'
import { useRouter } from 'next/router'
import { GenericPage, PagesApi } from 'plombr-front'
import React, { useEffect, useState } from 'react'

const DynamicPageImpl = (props) => {
  const { page } = props
  if (page.type === 'generic') {
    return <GenericPage items={page.items} />
  } else if (page.type != null) {
    return <span>404</span>
  }
  return null
}

const DynamicPage: NextPage<any> = ({ setTitle, ...props }) => {
  const [page, setPage] = useState<any>({})
  const router = useRouter()
  const { pathname, asPath } = router

  useEffect(() => {
    const doFetchPage = async () => {
      try {
        const { page } = await PagesApi.resolve(asPath)
        return page
      } catch (e) {
        if (e?.response.status === 404) {
          message.error(e?.response.data.detail)
          return {
            title: 'Not Found',
            type: '404'
          }
        } else {
          throw e
        }
      }
    }

    const doFetch = async () => {
      const page = await doFetchPage()
      setPage(page)
    }

    if (asPath !== pathname) {
      doFetch().then()
    }
  }, [asPath, pathname, setPage])

  const { title } = page
  return (
    <>
      <PageTitle title={title ?? 'Loading...'} />
      <Layout title={title}>
        <DynamicPageImpl page={page} {...props} />
      </Layout>
    </>
  )
}

export default DynamicPage
