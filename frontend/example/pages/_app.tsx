import { callApiAction, logoutAction, routeToAction, setTokenAction, showNotification } from '@/actions'
import { AuthApi } from '@/api/v1/auth'
import { getToken, refreshCurrentToken } from '@/auth'
import Image from '@/components/image'
import PageTitle from '@/components/page-title'
import 'antd/dist/antd.css'
import createAuthRefreshInterceptor from 'axios-auth-refresh'
import isArray from 'lodash/isArray'
import Head from 'next/head'
import Router from 'next/router'
import {
  ActionsCell,
  applyActions,
  Col,
  ColStore,
  DateTimeInput,
  DecrementIncrementIntegerCell,
  DeleteCell,
  Form,
  FormStore,
  getAxios,
  IntegerInput,
  Modal,
  ModalStore,
  OpenModalButton,
  PieChart,
  registerAction,
  registerComponent,
  Row,
  RowStore,
  Table,
  TableStore,
  Text,
  TextAreaInput,
  TextInput
} from 'plombr-front'
import 'plombr-front/lib/index.css'
import React from 'react'

registerComponent('modal', Modal, ModalStore)
registerComponent('row', Row, RowStore)
registerComponent('col', Col, ColStore)
registerComponent('open-modal-button', OpenModalButton)
registerComponent('table', Table, TableStore)
registerComponent('text', Text)
registerComponent('delete', DeleteCell)
registerComponent('actions', ActionsCell)
registerComponent('decrement-increment-integer', DecrementIncrementIntegerCell)
registerComponent('form', Form, FormStore)
registerComponent('text-input', TextInput)
registerComponent('textarea-input', TextAreaInput)
registerComponent('integer-input', IntegerInput)
registerComponent('datetime-input', DateTimeInput)
registerComponent('pie-chart', PieChart)
registerComponent('image', Image)

registerAction('logout', logoutAction)
registerAction('callApi', callApiAction)
registerAction('setToken', setTokenAction)
registerAction('routeTo', routeToAction)
registerAction('showNotification', showNotification)

const refreshAuthLogic = async (failedRequest) => {
  const token = await refreshCurrentToken()
  if (token == null) {
    await Router.push('/login')
  } else {
    failedRequest.response.config.headers.Authorization = `Bearer ${token.access}`
  }
}

createAuthRefreshInterceptor(getAxios(), refreshAuthLogic)

getAxios().interceptors.request.use(async (config) => {
  if (config.url === AuthApi.GetTokenUrl || config.url === AuthApi.RefreshTokenUrl) {
    return config
  }
  const token = await getToken()
  if (token) {
    config.headers.Authorization = `Bearer ${token.access}`
  }
  return config
})

async function tryApplyActions(actions) {
  if (isArray(actions)) {
    await applyActions(actions)
  }
}

getAxios().interceptors.response.use(
  async (response) => {
    const { data } = response
    await tryApplyActions(data?._actions)
    return response
  },
  async (err) => {
    await tryApplyActions(err?.response?.data?._actions)
    throw err
  }
)

export default function App({ Component, pageProps }) {
  return (
    <>
      <Head>
        <link rel='apple-touch-icon' sizes='180x180' href='/apple-touch-icon.png' />
        <link rel='icon' type='image/png' sizes='32x32' href='/favicon-32x32.png' />
        <link rel='icon' type='image/png' sizes='16x16' href='/favicon-16x16.png' />
        <link rel='manifest' href='/site.webmanifest' />
      </Head>
      <PageTitle title='' />
      <Component {...pageProps} />
    </>
  )
}
