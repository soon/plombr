import { clearToken, setToken } from '@/auth'
import { notification } from 'antd'
import Router from 'next/router'
import { getAxios } from 'plombr-front'

export function logoutAction() {
  clearToken()
  return Router.push('/login')
}

export async function callApiAction({ url, method }) {
  await getAxios().request({
    url,
    method
  })
}

export function setTokenAction({ access, refresh }) {
  setToken({ access, refresh })
}

export function routeToAction(url) {
  return Router.push(url)
}

export function showNotification({ message, description, duration, type }) {
  notification.open({
    message,
    description,
    placement: 'bottomRight',
    duration,
    type
  })
}
