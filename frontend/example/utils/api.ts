import useSWR from 'swr'

const fetcher = (url) => fetch(url).then((r) => r.json())
export const fetchWithParams = (url, params) => {
  const queryParams = new URLSearchParams(Object.entries(params)).toString()
  if (!queryParams) {
    return fetch(`${url}`)
  }
  return fetch(`${url}?${queryParams}`)
}

export function useApi<T>(x) {
  return useSWR<T>(x, fetcher)
}
