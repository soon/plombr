import { AuthApi, TokenResponse } from '@/api/v1/auth'
import jwtDecode, { JwtPayload } from 'jwt-decode'

type TokenInfo = {
  access: string
  refresh: string
  payload: JwtPayload & {
    exp: number
  }
}

export function setToken({ access, refresh }: TokenResponse): TokenInfo {
  if (typeof window === 'undefined') {
    return
  }
  const info: TokenInfo = {
    access,
    refresh,
    payload: jwtDecode(access)
  }
  localStorage.setItem('token', JSON.stringify(info))
  return info
}

function isExpired(token: TokenInfo): boolean {
  return Date.now() >= token.payload.exp * 1000
}

export function clearToken() {
  localStorage.removeItem('token')
}

export async function doGetToken(): Promise<TokenInfo | null> {
  if (typeof window === 'undefined') {
    return null
  }
  const value = localStorage.getItem('token')
  if (value) {
    try {
      return JSON.parse(value)
    } catch (e) {
      clearToken()
    }
  }
}

export async function getToken(): Promise<TokenInfo | null> {
  const token = await doGetToken()
  if (token != null && isExpired(token)) {
    return refreshToken(token.refresh)
  }
  return token
}

async function tryRefreshToken(refresh: string): Promise<TokenResponse | null> {
  try {
    return await AuthApi.refreshToken(refresh)
  } catch (e) {
    if (e?.response?.data?.code === 'token_not_valid') {
      return null
    }
    throw e
  }
}

export async function refreshToken(refresh: string): Promise<TokenInfo | null> {
  const newToken = await tryRefreshToken(refresh)
  if (newToken == null) {
    clearToken()
    return null
  }
  return setToken(newToken)
}

export async function refreshCurrentToken(): Promise<TokenInfo | null> {
  const token = await doGetToken()
  if (token != null) {
    return refreshToken(token.refresh)
  }
  return null
}
