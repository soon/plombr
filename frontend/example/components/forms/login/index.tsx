import { AuthApi } from '@/api/v1/auth'
import { setToken } from '@/auth'
import ActionButton from '@/components/buttons/action-button'
import { Button, Form, Input, Space } from 'antd'
import { useRouter } from 'next/router'
import * as React from 'react'
import { useState } from 'react'

export default function LoginForm() {
  const router = useRouter()
  const [loading, setLoading] = useState(false)

  const onFinish = async ({ username, password }) => {
    if (loading) {
      return
    }
    setLoading(true)
    try {
      const token = await AuthApi.getToken(username, password)
      setToken(token)
      await router.push('/')
    } catch (e) {
      console.error(e)
    } finally {
      setLoading(false)
    }
  }

  return (
    <Form
      layout='vertical'
      name='basic'
      initialValues={{ remember: true }}
      onFinish={onFinish}
      style={{
        maxWidth: '400px',
        width: '100%'
      }}
    >
      <Form.Item label='Username' name='username' rules={[{ required: true, message: 'Please input your username!' }]}>
        <Input />
      </Form.Item>

      <Form.Item label='Password' name='password' rules={[{ required: true, message: 'Please input your password!' }]}>
        <Input.Password />
      </Form.Item>

      <Form.Item>
        <Space>
          <Button type='primary' htmlType='submit' loading={loading}>
            Login
          </Button>
          <ActionButton
            name='callApi'
            data={{
              url: '/api/users/generate',
              method: 'post'
            }}
          >
            Generate Temporary User
          </ActionButton>
        </Space>
      </Form.Item>
    </Form>
  )
}
