import { Button } from 'antd'
import { ButtonProps } from 'antd/lib/button/button'
import { applyAction } from 'plombr-front'
import React, { useState } from 'react'

type Props = ButtonProps & {
  name: string
  data?: any
}

export default function ActionButton({ name, data, ...props }: Props) {
  const [loading, setLoading] = useState(false)

  const onClick = async () => {
    if (loading) {
      return
    }
    setLoading(true)
    try {
      await applyAction(name, data)
    } finally {
      setLoading(false)
    }
  }

  return <Button {...props} onClick={onClick} loading={loading} />
}
