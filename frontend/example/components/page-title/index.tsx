import Head from 'next/head'
import * as React from 'react'

export default function PageTitle({ title }: { title: string }) {
  return (
    <Head>
      <title>{title ? `${title} • Plombr` : 'Plombr'}</title>
    </Head>
  )
}
