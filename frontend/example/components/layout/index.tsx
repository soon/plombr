import { useApi } from '@/utils/api'
import { Layout as AntdLayout } from 'antd'
import { DynamicSidebar } from 'plombr-front'
import React from 'react'
import css from './style.module.scss'

const { Header, Content, Footer, Sider } = AntdLayout

export default function Layout({ children, title }) {
  const { data, error } = useApi<any>('/api/v1/menu/items')

  return (
    <AntdLayout style={{ minHeight: '100vh' }}>
      <Sider className={css.sider} theme='light'>
        <div className={css.logo}>PLOMBR</div>
        {error ? <span>{`${error}`}</span> : <DynamicSidebar className={css.menu} items={(data && data.items) || []} />}
      </Sider>
      <AntdLayout className={css.main}>
        <Header className={css.header}>
          <h1 className={css.title}>{title}</h1>
        </Header>
        <Content style={{ margin: 16 }}>{children}</Content>
        <Footer className={css.footer}>Made with 🔥 and 🍷 and ❤️</Footer>
      </AntdLayout>
    </AntdLayout>
  )
}
