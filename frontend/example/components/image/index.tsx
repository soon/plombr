import { Image as AntdImage } from 'antd'
import React from 'react'

type Props = {
  data: string
}

export default function Image({ data }: Props) {
  return (
    <div style={{ maxWidth: 200, maxHeight: 200 }}>
      <AntdImage src={data} alt='image' />
    </div>
  )
}
