const { PHASE_DEVELOPMENT_SERVER } = require('next/constants')

module.exports = (phase, { defaultConfig }) => {
  if (phase === PHASE_DEVELOPMENT_SERVER) {
    return {
      async rewrites() {
        return [
          {
            source: '/api/:path*',
            destination: 'http://localhost:8000/api/:path*'
          },
          {
            source: '/media/:path*',
            destination: 'http://localhost:8000/media/:path*'
          }
        ]
      },
      experimental: {
        optionalCatchAll: true
      }
    }
  }
  return {}
}
