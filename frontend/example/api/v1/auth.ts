import { AxiosInstance } from 'axios'
import { AxiosAuthRefreshRequestConfig } from 'axios-auth-refresh'
import { getAxios } from 'plombr-front'

export type TokenResponse = {
  refresh: string
  access: string
}

class AuthApiImpl {
  protected axios: AxiosInstance
  public readonly GetTokenUrl = '/api/v1/auth/token'
  public readonly RefreshTokenUrl = '/api/v1/auth/token/refresh'

  constructor() {
    this.axios = getAxios()
  }

  public async getToken(username: string, password: string): Promise<TokenResponse> {
    const config: AxiosAuthRefreshRequestConfig = {
      skipAuthRefresh: true
    }
    const response = await this.axios.post(
      this.GetTokenUrl,
      {
        username,
        password
      },
      config
    )
    return response.data
  }

  public async refreshToken(refresh: string): Promise<TokenResponse> {
    const config: AxiosAuthRefreshRequestConfig = {
      skipAuthRefresh: true
    }
    const { data } = await this.axios.post(
      this.RefreshTokenUrl,
      {
        refresh
      },
      config
    )
    return {
      refresh,
      access: data.access
    }
  }
}

export const AuthApi = new AuthApiImpl()
