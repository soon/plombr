# plombr-front

> 

[![NPM](https://img.shields.io/npm/v/plombr-front.svg)](https://www.npmjs.com/package/plombr-front) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save plombr-front
```

## Usage

```tsx
import React, { Component } from 'react'

import MyComponent from 'plombr-front'
import 'plombr-front/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

 © [](https://github.com/)
