from django.apps import AppConfig


__all__ = ['ListsConfig']


class ListsConfig(AppConfig):
    name = 'plombr.lists'
    label = 'plombr_lists'
