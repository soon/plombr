import typing
from typing import Any

from plombr.components.base import Component
from plombr.lists.api import serializers


class List(Component):
    kind = 'list'
    serializer_class = serializers.ListSerializer
    item_kind = None
    item_serializer_class = None

    def __init__(self, *, items=None, **kwargs):
        super().__init__(**kwargs)
        if not items:
            items = []
        self.items = items

    def get_items(self) -> typing.List[Any]:
        return self.items

    def serialize_items(self):
        serializer = self.item_serializer_class(many=True)
        return serializer.to_representation(self.get_items())
