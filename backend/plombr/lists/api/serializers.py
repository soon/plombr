from plombr.components.serializers import ComponentSerializer
from rest_framework.fields import CharField, SerializerMethodField


class ListSerializer(ComponentSerializer):
    item_kind = CharField()
    items = SerializerMethodField()

    def get_items(self, obj):
        return obj.serialize_items()
