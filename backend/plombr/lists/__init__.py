from plombr.lists.list import List


default_app_config = 'plombr.lists.apps.ListsConfig'

__all__ = ['default_app_config', 'List']
