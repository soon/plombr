from typing import Any, Optional


class Action:
    def __init__(self, name: str, data: Optional[Any] = None):
        self.name = name
        self.data = data
