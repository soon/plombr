from rest_framework.fields import CharField, Field

from plombr.utils.serializers import CamelCaseSerializer


class AnyField(Field):
    def to_representation(self, value):
        return value


class ActionSerializer(CamelCaseSerializer):
    name = CharField()
    data = AnyField(required=False)
