from django.apps import AppConfig


class Config(AppConfig):
    name = 'plombr.forms.tests'
    label = 'plombr.forms.tests'


default_app_config = 'plombr.forms.tests.Config'
