from django import forms

from plombr.forms.layout import FormLayout


def get_layout_meta_fields_as_dict(layout: FormLayout):
    fields = sorted([x for x in dir(layout._meta) if not x.startswith('_')])
    return {x: getattr(layout._meta, x) for x in fields}


class TestFormLayout:
    def test_regular_form(self):
        class CustomForm(forms.Form):
            integer_field = forms.IntegerField()
            char_field = forms.CharField()

        class Layout(FormLayout):
            class Meta:
                form_class = CustomForm

        layout = Layout()
        assert list(layout.fields.keys()) == [
            'integer_field',
            'char_field',
        ]
        assert get_layout_meta_fields_as_dict(layout) == {
            'app_label': 'plombr.forms.tests',
            'fields': None,
            'form_class': CustomForm,
            'label': 'plombr.forms.tests.Layout',
            'object_name': 'Layout',
            'submit_url': '/api/v1/forms/plombr.forms.tests.Layout/submit',
        }
