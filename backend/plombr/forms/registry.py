from typing import Dict, Type

from django import forms
from django.db import models

from plombr.forms import fields


MODEL_FIELD_MAPPING: Dict[Type[models.Field], Type[fields.FormField]] = {
    models.CharField: fields.CharField,
    models.TextField: fields.TextAreaField,
    models.DateTimeField: fields.DateTimeField,
    models.ImageField: fields.ImageField,
}

FORM_FIELD_MAPPING: Dict[Type[forms.Field], Type[fields.FormField]] = {
    forms.IntegerField: fields.IntegerField,
    forms.CharField: fields.CharField,
    forms.DateTimeField: fields.DateTimeField,
    forms.ImageField: fields.ImageField,
}


class FormLayoutRegistry:
    def __init__(self):
        self.forms = {}

    def add(self, layout_class):
        key = layout_class._meta.label
        if key in self.forms:
            raise ValueError(f'A form with label {key} is already registered')
        self.forms[key] = layout_class

    def get(self, key):
        return self.forms[key]


registry = FormLayoutRegistry()
