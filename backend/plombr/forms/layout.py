import urllib.parse
from typing import Any, Dict, List, Tuple

from django.apps import apps
from django.core.exceptions import ImproperlyConfigured
from django.urls import reverse_lazy

from plombr.forms.api.serializers import FormSerializer
from plombr.forms.base import FormElement
from plombr.forms.registry import FORM_FIELD_MAPPING, registry
from plombr.utils.objects import get_object_value


class FormLayoutOptions:
    def __init__(self, meta_class=None, app_label=None, object_name=None):
        try:
            self.form_class = getattr(meta_class, 'form_class')
        except AttributeError:
            raise ImproperlyConfigured('Meta.form_class is required')
        self.fields = getattr(meta_class, 'fields', None)
        self.app_label = getattr(meta_class, 'app_label', app_label)
        self.object_name = object_name
        self.submit_url = getattr(meta_class, 'submit_url', reverse_lazy('plombr-forms:submit', args=(self.label,)))

    @property
    def label(self):
        return f'{self.app_label}.{self.object_name}'


class FormLayoutMeta(type):
    reserved_fields = (
        'fields',
        '_fields',
        '_meta',
        '_form',
        'serializer_class',
    )

    def __new__(mcs, name, bases, attrs):
        parents = [b for b in bases if isinstance(b, FormLayoutMeta)]
        if not parents or attrs.get('_abstract'):
            return super().__new__(mcs, name, bases, attrs)

        try:
            return mcs.do_new(name, bases, attrs)
        except ImproperlyConfigured as exc:
            raise ImproperlyConfigured(f'{exc}, form layout: {name}')

    @classmethod
    def do_new(mcs, name, bases, attrs):
        mcs.check_reserved_fields(attrs)
        fields, new_attrs = mcs.extract_fields(attrs)
        meta = mcs.prepare_meta(name, attrs)
        new_attrs['_meta'] = meta

        form_fields = meta.fields
        if form_fields is None:
            form_fields = list(meta.form_class.base_fields) + list(meta.form_class.declared_fields)

        final_fields = mcs.merge_fields(meta.form_class, form_fields, fields)
        new_attrs['_fields'] = final_fields

        new_class = super().__new__(mcs, name, bases, new_attrs)
        registry.add(new_class)
        return new_class

    @classmethod
    def prepare_meta(mcs, name, attrs):
        try:
            meta_config = attrs['Meta']
        except KeyError:
            raise ImproperlyConfigured(f'Meta class not found')
        module = attrs['__module__']
        app_config = apps.get_containing_app_config(module)
        return FormLayoutOptions(
            meta_config,
            app_label=app_config.label,
            object_name=name,
        )

    @classmethod
    def check_reserved_fields(mcs, attrs):
        invalid_fields = [x for x in mcs.reserved_fields if x in attrs]
        if invalid_fields:
            msg = ', '.join(invalid_fields) + (' is' if len(invalid_fields) == 1 else ' are')
            raise ImproperlyConfigured(f'{msg} reserved')

    @classmethod
    def extract_fields(mcs, attrs) -> Tuple[Dict[str, FormElement], Dict[str, Any]]:
        new_attrs = {}
        fields = {}
        for name, item in attrs.items():
            if isinstance(item, FormElement):
                fields[name] = item
                item.name = name
            else:
                new_attrs[name] = item
        return fields, new_attrs

    @classmethod
    def merge_fields(
        mcs, form_class, form_fields: List[str], layout_fields: Dict[str, FormElement]
    ) -> Dict[str, FormElement]:
        res = {}
        for name in form_fields:
            if name in layout_fields:
                field = layout_fields[name]
            else:
                try:
                    form_field = form_class.declared_fields[name]
                except KeyError:
                    form_field = form_class.base_fields[name]
                field = mcs.construct_form_element(name, form_field)
            res[name] = field
        return res

    @classmethod
    def construct_form_element(mcs, name, field) -> FormElement:
        try:
            form_field_class = FORM_FIELD_MAPPING[type(field)]
        except KeyError:
            raise ImproperlyConfigured(
                f'{type(field)} is not registered in form layout elements mapping (field name: {name})'
            )
        return form_field_class(
            name=name,
            required=field.required,
            field=field,
        )


class FormLayout(FormElement, metaclass=FormLayoutMeta):
    kind = 'form'
    object_id = None
    serializer_class = FormSerializer

    def __init__(self, request, **kwargs):
        self.request = request
        super().__init__(**kwargs)

    @property
    def fields(self):
        return self._fields

    @property
    def fields_list(self):
        return list(self.fields.values())

    def construct_form(self):
        return self._meta.form_class(**self.get_form_kwargs())

    def do_get_object(self):
        if self.request.method == 'PATCH':
            return self.get_object()

    def get_model(self):
        return self._meta.form_class._meta.model

    def get_object(self):
        if self.object_id is None:
            return None
        model = self.get_model()
        return model.objects.get(pk=self.object_id)

    def get_form_kwargs(self):
        return {'data': self.request.data, 'instance': self.do_get_object()}

    def get_submit_url(self):
        url = self._meta.submit_url
        if self.object_id is not None:
            url += '?' + urllib.parse.urlencode({'id': self.object_id})
        return url

    def get_expected_submit_method(self):
        return 'POST' if self.object_id is None else 'PATCH'

    def get_success_response(self):
        return {'message': {'success': 'Form saved!'}}

    def get_object_value(self, obj, field_name):
        return get_object_value(obj, [field_name])
