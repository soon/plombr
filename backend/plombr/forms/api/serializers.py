from rest_framework.fields import BooleanField, CharField, ListField
from rest_framework.serializers import Serializer

from plombr.components.serializers import ComponentSerializer
from plombr.utils.objects import get_object_value
from plombr.utils.serializers import DynamicField


class FormElementSerializer(ComponentSerializer):
    name = CharField()
    label = CharField(source='get_label')
    help_text = CharField()
    required = BooleanField()
    readonly = BooleanField()


class SelectFieldSerializer(FormElementSerializer):
    data_url = CharField()
    choices = ListField(ListField(child=CharField()))


class TableFieldSerializer(FormElementSerializer):
    row = DynamicField(source='row_field')


class FormSetSerializer(FormElementSerializer):
    fields = ListField(
        child=DynamicField(),
        source='fields_list',
    )


class FormObjectSerializer(Serializer):
    def to_representation(self, instance):
        obj = instance.get_object()
        if obj is None:
            return None
        return {name: f.serialize_value(instance.get_object_value(obj, name)) for name, f in instance.fields.items()}


class FormSerializer(FormElementSerializer):
    items = ListField(
        child=DynamicField(),
        source='fields_list',
    )
    submit_url = CharField(
        source='get_submit_url',
    )
    submit_method = CharField(source='get_expected_submit_method')
    object = FormObjectSerializer(source='*')
