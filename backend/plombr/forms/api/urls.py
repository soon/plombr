from django.urls import path

from plombr.forms.api import views


app_name = 'plombr-forms'

urlpatterns = [path('<str:key>/submit', views.SubmitFormApiView.as_view(), name='submit')]
