from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from plombr.forms.layout import FormLayout
from plombr.forms.registry import registry


class FormApiMixin:
    def get_layout(self, request, key, **kwargs) -> FormLayout:
        layout_class = registry.get(key)
        return layout_class(request, **kwargs)


class SubmitFormApiView(FormApiMixin, APIView):
    def post(self, request, *args, key, **kwargs):
        return self.handle(request, key, None)

    def patch(self, request, *args, key, **kwargs):
        object_id = request.query_params['id']
        return self.handle(request, key, object_id)

    def handle(self, request, key, object_id):
        layout = self.get_layout(request, key, object_id=object_id)
        form = layout.construct_form()
        if form.is_valid():
            form.save()
            response = layout.get_success_response()
            return Response(response)
        else:
            return Response({'errors': {'form': form.errors}}, status=status.HTTP_400_BAD_REQUEST)
