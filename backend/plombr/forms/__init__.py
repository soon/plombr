from django.utils.module_loading import autodiscover_modules


default_app_config = 'plombr.forms.apps.FormConfig'


def autodiscover():
    autodiscover_modules('forms')
