from django.apps import AppConfig


__all__ = ['FormConfig']


class FormConfig(AppConfig):
    name = 'plombr.forms'
    label = 'plombr_forms'

    def ready(self):
        self.module.autodiscover()
