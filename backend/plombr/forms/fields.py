from plombr.forms.api.serializers import FormSetSerializer, SelectFieldSerializer, TableFieldSerializer
from plombr.forms.base import FormElement


class FormField(FormElement):
    pass


class IntegerField(FormField):
    kind = 'integer-input'


class CheckboxField(FormField):
    kind = 'checkbox-input'


class SelectField(FormField):
    kind = 'select-input'
    data_url = None
    choices = None
    serializer_class = SelectFieldSerializer


class CharField(FormField):
    kind = 'text-input'


class TextAreaField(FormField):
    kind = 'textarea-input'


class DateTimeField(FormField):
    kind = 'datetime-input'


class ImageField(FormField):
    kind = 'image-input'


class TableField(FormField):
    kind = 'table-field'
    row_field = None
    serializer_class = TableFieldSerializer

    def __init__(self, row_field: FormElement, **kwargs):
        super().__init__(row_field=row_field, **kwargs)


class FormSetField(FormField):
    kind = 'formset'
    serializer_class = FormSetSerializer

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
