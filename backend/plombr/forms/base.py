from typing import Any, Dict

import stringcase

from plombr.components.base import Component
from plombr.forms.api.serializers import FormElementSerializer
from plombr.utils.humanize import name_to_label


class FormElement(Component):
    name = None
    label = None
    help_text = None
    required = False
    readonly = False
    serializer_class = FormElementSerializer

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    @property
    def name_as_label(self):
        if not self.name:
            return ''
        return name_to_label(self.name)

    def get_label(self):
        return self.label or self.name_as_label

    def serialize_value(self, value):
        return value
