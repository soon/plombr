from typing import List

from plombr.components.base import Component
from plombr.pages.api.serializers import ChartComponentSerializer, ColSerializer, RowSerializer, PieChartSerializer


class BasePageContainerComponent(Component):
    def __init__(self, *items: Component):
        self.items = list(items)

    def get_items(self) -> List[Component]:
        return self.items


class Row(BasePageContainerComponent):
    kind = 'row'
    serializer_class = RowSerializer

    @classmethod
    def wrap_col(cls, item: Component):
        if item.kind == 'col':
            return item
        return Col(item)

    def get_items(self) -> List[Component]:
        return [self.wrap_col(x) for x in super().get_items()]


class Col(BasePageContainerComponent):
    kind = 'col'
    serializer_class = ColSerializer


class Chart(Component):
    kind = 'chart'
    serializer_class = ChartComponentSerializer


class PieChart(Component):
    kind = 'pie-chart'
    serializer_class = PieChartSerializer
