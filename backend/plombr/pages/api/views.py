from django.http import Http404
from rest_framework.response import Response
from rest_framework.views import APIView

from plombr.pages.api.serializers import ResolvePageResponseSerializer
from plombr.pages.page import Page
from plombr.pages.urls import resolve_page


class ResolvePageApiView(APIView):
    def check_page_permissions(self, request, page):
        for permission in page.get_permissions():
            if not permission.has_permission(request, self):
                self.permission_denied(
                    request, message=getattr(permission, 'message', None), code=getattr(permission, 'code', None)
                )

    def get(self, request, *args, **kwargs):
        url = request.query_params.get('url')
        res = resolve_page(url)
        func = res.func
        if not issubclass(func, Page):
            raise Http404

        page = func(request=request, args=res.args, kwargs=res.kwargs)
        page.init()
        self.check_page_permissions(request, page)
        serializer = ResolvePageResponseSerializer({'page': page})
        return Response(serializer.data)
