from rest_framework.fields import CharField, ListField

from plombr.components.serializers import ComponentSerializer
from plombr.tables.api.serializers import TableSerializer
from plombr.utils.serializers import CamelCaseSerializer, DynamicField


class BasePageContainerComponentSerializer(ComponentSerializer):
    items = ListField(child=DynamicField())


class RowSerializer(BasePageContainerComponentSerializer):
    pass


class ColSerializer(BasePageContainerComponentSerializer):
    pass


class FormComponentSerializer(ComponentSerializer):
    form = DynamicField(source='get_form')


class ChartComponentSerializer(ComponentSerializer):
    pass


class PieChartSerializer(ChartComponentSerializer):
    data_url = CharField()


class PageSerializer(CamelCaseSerializer):
    type = CharField()
    title = CharField(source='get_title')


class GenericPageSerializer(PageSerializer):
    items = ListField(
        child=DynamicField(),
        source='get_items',
    )


class ResolvePageResponseSerializer(CamelCaseSerializer):
    page = DynamicField()
