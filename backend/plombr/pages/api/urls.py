from django.urls import path

from plombr.pages.api import views


app_name = 'page'

urlpatterns = [path('resolve', views.ResolvePageApiView.as_view(), name='resolve')]
