from typing import List

from django.http import HttpRequest

from plombr.components.base import Component
from plombr.pages.api.serializers import GenericPageSerializer
from plombr.tables import Table


class Page:
    type = None
    title = None
    args = None
    kwargs = None
    serializer_class = None
    request: HttpRequest
    permission_classes = ()

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    def get_title(self):
        return self.title

    def get_permissions(self):
        return [x() for x in self.permission_classes]

    def init(self):
        pass


class GenericPage(Page):
    type = 'generic'
    serializer_class = GenericPageSerializer

    def get_items(self) -> List[Component]:
        return []


class TablePage(GenericPage):
    table_class = None
    table: Table

    def init(self):
        self.table = self.table_class(self.request)

    def get_permissions(self):
        return list(super().get_permissions()) + list(self.table.get_permissions())

    def get_items(self):
        return [self.table]

    def get_title(self):
        title = super().get_title()
        if not title:
            title = self.table_class.get_model_class_plural()
            if title:
                title = title.capitalize()
        return title


class FormLayoutPage(GenericPage):
    form_layout_class = None

    def get_form_kwargs(self):
        return {}

    def get_items(self):
        return [
            self.form_layout_class(self.request, **self.get_form_kwargs()),
        ]
