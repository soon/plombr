from django.conf import settings
from django.urls import resolve, reverse
from django.utils.functional import lazy


def reverse_page(viewname, args=None, kwargs=None, current_app=None):
    return reverse(
        viewname,
        urlconf=settings.PAGES_URLCONF,
        args=args,
        kwargs=kwargs,
        current_app=current_app,
    )


reverse_page_lazy = lazy(reverse_page, str)


def resolve_page(path):
    return resolve(path, urlconf=settings.PAGES_URLCONF)
