from plombr.components import serializers
from plombr.components.base import Component


class Text(Component):
    serializer_class = serializers.DataComponentSerializer

    kind = 'text'
    data = ''
