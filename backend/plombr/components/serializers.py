from rest_framework.fields import CharField

from plombr.utils.serializers import CamelCaseSerializer, MaybeDynamicField


class ComponentSerializer(CamelCaseSerializer):
    kind = CharField()


class DataComponentSerializer(ComponentSerializer):
    data = MaybeDynamicField(fallback=CharField(allow_null=True))
