from uuid import uuid4

from plombr.components.base import Component
from plombr.components.modals import serializers


class Modal(Component):
    kind = 'modal'
    serializer_class = serializers.ModalSerializer

    def __init__(self, title=None, body=None, key=None):
        self.title = title or ''
        self.body = body
        self.key = key or str(uuid4())


class OpenModalButton(Component):
    kind = 'open-modal-button'
    serializer_class = serializers.OpenModalButtonSerializer

    def __init__(self, title, modal):
        self.title = title
        self.modal_key = modal.key if isinstance(modal, Modal) else str(modal)
