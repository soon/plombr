from rest_framework.fields import CharField

from plombr.components.serializers import ComponentSerializer
from plombr.utils.serializers import MaybeDynamicField


class ModalSerializer(ComponentSerializer):
    title = CharField(allow_blank=True)
    body = MaybeDynamicField(fallback=CharField(allow_null=True))
    modal_key = CharField(source='key')


class OpenModalButtonSerializer(ComponentSerializer):
    title = CharField(allow_blank=True)
    modal_key = CharField()
