from collections import Mapping


def get_object_value(obj, path, *, default=None):
    if isinstance(path, str):
        path = path.split('.')

    value = obj
    for p in path:
        if hasattr(value, p):
            value = getattr(value, p)
        elif isinstance(value, Mapping) and p in value:
            value = value[p]
        else:
            return default
    return value
