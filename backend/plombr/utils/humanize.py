def name_to_label(name: str) -> str:
    return ' '.join(x.capitalize() for x in name.replace('_', ' ').split())
