import inspect

import stringcase
from rest_framework.fields import Field
from rest_framework.serializers import Serializer


class CamelCaseSerializer(Serializer):
    def to_representation(self, instance):
        res = super().to_representation(instance)
        return {stringcase.camelcase(k): v for k, v in res.items()}


class DynamicField(Field):
    def to_representation(self, value):
        if not callable(value.serializer_class):
            raise ValueError(f'{type(value).__name__}.serializer_class must be callable ({value.serializer_class})')
        serializer = value.serializer_class()
        return serializer.to_representation(value)


class MaybeDynamicField(Field):
    def __init__(self, *args, fallback, **kwargs):
        super().__init__(*args, **kwargs)
        self.fallback = fallback
        assert not inspect.isclass(self.fallback), '`fallback` has not been instantiated.'

    def to_representation(self, value):
        if hasattr(value, 'serializer_class'):
            serializer = value.serializer_class()
        else:
            serializer = self.fallback
        return serializer.to_representation(value)
