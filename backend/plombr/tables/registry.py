class TableRegistry:
    def __init__(self):
        self.tables = {}

    def add(self, table_class):
        key = table_class._meta.label
        if key in self.tables:
            raise ValueError(f'A table with label {key} is already registered')
        self.tables[key] = table_class

    def get(self, key):
        return self.tables[key]


registry = TableRegistry()
