import abc
from typing import Dict

from django.apps import apps
from django.core.exceptions import FieldDoesNotExist, ImproperlyConfigured
from django.db import models
from django.urls import reverse_lazy

from plombr.components.base import Component
from plombr.tables.api.serializers import TableSerializer
from plombr.tables.registry import registry
from plombr.utils.humanize import name_to_label
from plombr.utils.objects import get_object_value


class TableColumnFilter(abc.ABC):
    name = ''
    label = ''

    @abc.abstractmethod
    def apply(self, qs):
        return qs


class TableColumn:
    type = None
    name = None
    label = None
    sortable = True
    filters = None
    searchable = True
    widget = None
    serializer_class = None

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    @property
    def name_as_label(self):
        if not self.name:
            return ''
        return name_to_label(self.name)

    def get_label(self):
        if self.label is None:
            return self.name_as_label
        return self.label

    def prepare_value(self, row):
        return get_object_value(row, [self.name])

    def serialize_value(self, value):
        if self.serializer_class and value is not None:
            value = self.serializer_class().to_representation(value)
        return value


class TableAction(abc.ABC):
    name = ''
    label = ''
    success_message = 'Action has been applied!'

    @abc.abstractmethod
    def apply(self, qs):
        pass

    def get_success_message(self):
        return self.success_message


class DeleteTableAction(TableAction):
    name = 'delete'
    label = 'Delete'
    success_message = 'Object has been deleted!'

    def apply(self, qs):
        qs.delete()


class TableOptions:
    def __init__(self, meta_class=None, app_label=None, object_name=None):
        self.add_new_url = getattr(meta_class, 'add_new_url', None)
        self.actions = list(getattr(meta_class, 'actions', None) or [])
        self.actions_dict = {x.name: x for x in self.actions}
        self.app_label = getattr(meta_class, 'app_label', app_label)
        self.object_name = object_name
        self.data_url = getattr(meta_class, 'data_url', reverse_lazy('table:data', args=(self.label,)))
        self.action_url = getattr(meta_class, 'action_url', reverse_lazy('table:action', args=(self.label,)))
        self.permission_classes = getattr(meta_class, 'permission_classes', ())

    @property
    def label(self):
        return f'{self.app_label}.{self.object_name}'


class TableMeta(type):
    def __new__(mcs, name, bases, attrs):
        parents = [b for b in bases if isinstance(b, TableMeta)]
        if not parents or attrs.get('_abstract'):
            return super().__new__(mcs, name, bases, attrs)

        module = attrs['__module__']
        app_config = apps.get_containing_app_config(module)

        columns, new_attrs = mcs.extract_columns_and_other_attrs(attrs, module)
        if '_columns' in new_attrs:
            raise ValueError('_columns is reserved')
        new_attrs['_columns'] = columns
        new_attrs.pop('_abstract', None)
        new_class = super().__new__(mcs, name, bases, new_attrs)
        new_class._meta = TableOptions(
            meta_class=getattr(new_class, 'Meta', None),
            app_label=app_config.label,
            object_name=name,
        )
        registry.add(new_class)
        return new_class

    @classmethod
    def extract_columns_and_other_attrs(mcs, attrs, module):
        new_attrs = {'__module__': module}
        columns = {}
        for name, item in attrs.items():
            if isinstance(item, TableColumn):
                columns[name] = item
                item.name = name
            else:
                new_attrs[name] = item
        return columns, new_attrs


class Table(Component, metaclass=TableMeta):
    kind = 'table'
    data_url = None
    queryset = None
    builtin_actions = (DeleteTableAction,)
    serializer_class = TableSerializer

    def __init__(self, request, **kwargs):
        self.request = request
        super().__init__(**kwargs)
        self._builtin_actions_dict = {action.name: action for action in self.builtin_actions}

    @property
    def columns(self) -> Dict[str, 'TableColumn']:
        return self._columns

    def do_get_queryset(self):
        if self.queryset is not None:
            return self.queryset.all()

    def get_queryset(self):
        qs = self.do_get_queryset()
        if qs is None:
            raise ImproperlyConfigured('queryset is not defined')
        return qs

    def columns_list(self):
        return list(self.columns.values())

    def get_action_by_name(self, name) -> TableAction:
        try:
            action = self._meta.actions_dict[name]
        except KeyError:
            action = self._builtin_actions_dict[name]
        return action()

    def serialize_rows(self, rows):
        return [self.serialize_row(r) for r in rows]

    def serialize_row(self, row):
        return {col.name: self.get_column_value(row, col) for col in self.columns.values()}

    def get_column_value(self, row, col: TableColumn):
        getter_name = f'prepare_{col.name}'
        getter = getattr(self, getter_name, None)
        if callable(getter):
            return getter(row)
        value = col.prepare_value(row)
        return col.serialize_value(value)

    def get_permissions(self):
        return [x() for x in self._meta.permission_classes]


class Column(TableColumn):
    type = 'text'


class ActionsColumn(TableColumn):
    type = 'actions'
    searchable = False


class DeleteColumn(TableColumn):
    type = 'delete'
    searchable = False


class ModelTableOptions(TableOptions):
    def __init__(self, meta_class=None, app_label=None, object_name=None):
        super().__init__(meta_class, app_label, object_name)
        self.model = getattr(meta_class, 'model', None)
        if not issubclass(self.model, models.Model):
            raise ImproperlyConfigured(f'Meta.model ({self.model}) must be subclass of models.Meta')
        self.fields = getattr(meta_class, 'fields', None)
        if self.fields is None:
            raise ImproperlyConfigured(f'Meta.fields must be specified')

    def get_model_field(self, field_name):
        return self.model._meta.get_field(field_name)


class ModelTableMeta(TableMeta):
    def __new__(mcs, name, bases, attrs):
        parents = [b for b in bases if isinstance(b, ModelTableMeta)]
        if not parents:
            attrs['_abstract'] = True

        new_class = super().__new__(mcs, name, bases, attrs)
        meta_config = getattr(new_class, 'Meta', None)
        if meta_config is None:
            return new_class

        try:
            meta = ModelTableOptions(meta_config, new_class._meta.app_label, new_class._meta.object_name)
        except ImproperlyConfigured as exc:
            raise ImproperlyConfigured(f'{exc}, table: {new_class}')
        new_class._meta = meta

        new_columns = {}
        for column_name in meta.fields:
            if column_name in new_class._columns:
                new_columns[column_name] = new_class._columns[column_name]
                continue
            try:
                model_field = meta.get_model_field(column_name)
            except FieldDoesNotExist as exc:
                raise ImproperlyConfigured(
                    f'Model {meta.model} does not have field {column_name}, form: {new_class}'
                ) from exc
            column_class = Column
            new_columns[column_name] = column_class(
                name=column_name,
                label=name_to_label(model_field.verbose_name),
                searchable=not model_field.is_relation,
                sortable=not model_field.is_relation,
            )
        for column_name, column in new_class._columns.items():
            if column_name not in new_columns:
                new_columns[column_name] = column
        new_class._columns = new_columns
        return new_class


class ModelTable(Table, metaclass=ModelTableMeta):
    def do_get_queryset(self):
        qs = super().do_get_queryset()
        if qs is None:
            if self._meta.model is not None:
                return self._meta.model.objects.all()

    @classmethod
    def get_model_class(cls):
        return cls._meta.model

    @classmethod
    def get_model_class_plural(cls):
        model = cls.get_model_class()
        return model._meta.verbose_name_plural
