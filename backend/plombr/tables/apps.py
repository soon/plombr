from django.apps import AppConfig


__all__ = ['TableConfig']


class TableConfig(AppConfig):
    name = 'plombr.tables'

    def ready(self):
        self.module.autodiscover()
