from plombr.tables.api.serializers import LinkActionSerializer, RowActionSerializer


class BaseRowAction:
    kind = None
    serializer_class = None

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    def serialize(self):
        return self.serializer_class(self).data


class Link(BaseRowAction):
    kind = 'link'
    text = None
    url = None
    serializer_class = LinkActionSerializer


class RowAction(BaseRowAction):
    kind = 'action'
    text = None
    table_action_key = None
    serializer_class = RowActionSerializer
