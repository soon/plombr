from django.urls import path

from plombr.tables.api import views


app_name = 'table'

urlpatterns = [
    path('<str:key>/data', views.TableDataApiView.as_view(), name='data'),
    path('<str:key>/action', views.TableActionApiView.as_view(), name='action'),
]
