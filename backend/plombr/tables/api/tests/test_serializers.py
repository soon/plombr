from plombr.tables import Column, Table, TableColumnFilter
from plombr.tables.api.serializers import TableSerializer
from plombr.tables.table import TableAction
from plombr.tables.widgets import IncrementDecrementIntegerWidget


class TestTableSerializer:
    def test_complex_table(self):
        class CustomTableAction(TableAction):
            name = 'table_action'
            label = 'Table Action'

            def apply(self, qs):
                pass

        class FirstFilter(TableColumnFilter):
            name = 'first_filter'
            label = 'First Filter'

            def apply(self, qs):
                return qs

        class SecondFilter(TableColumnFilter):
            name = 'second_filter'
            label = 'Second Filter'

            def apply(self, qs):
                return qs

        class CustomTable(Table):
            text = Column(sortable=False)
            underscored_name = Column(
                searchable=False,
                filters=[
                    FirstFilter(),
                    SecondFilter(),
                ],
            )
            with_widget = Column(widget=IncrementDecrementIntegerWidget())

            class Meta:
                add_new_url = '/add/new'
                actions = [CustomTableAction]

        serializer = TableSerializer(CustomTable())
        assert serializer.data == {
            'columns': [
                {
                    'type': 'text',
                    'name': 'text',
                    'label': 'Text',
                    'sortable': False,
                    'filters': None,
                    'searchable': True,
                    'widget': None,
                },
                {
                    'type': 'text',
                    'name': 'underscored_name',
                    'label': 'Underscored Name',
                    'sortable': True,
                    'filters': [
                        {
                            'name': 'first_filter',
                            'label': 'First Filter',
                        },
                        {
                            'name': 'second_filter',
                            'label': 'Second Filter',
                        },
                    ],
                    'searchable': False,
                    'widget': None,
                },
                {
                    'type': 'text',
                    'name': 'with_widget',
                    'label': 'With Widget',
                    'sortable': True,
                    'filters': None,
                    'searchable': True,
                    'widget': {
                        'kind': 'decrement-increment-integer',
                    },
                },
            ],
            'actions': [
                {
                    'name': 'table_action',
                    'label': 'Table Action',
                }
            ],
            'dataUrl': '/api/v1/tables/tests.CustomTable/data',
            'actionUrl': '/api/v1/tables/tests.CustomTable/action',
            'addNewUrl': '/add/new',
        }
