import operator
from functools import reduce
from typing import Optional, Type, cast

from django.db.models import Q
from django_filters import BooleanFilter, CharFilter, OrderingFilter, rest_framework as filters
from rest_framework.response import Response
from rest_framework.views import APIView

from plombr.tables import Table
from plombr.tables.api.serializers import ApplyTableActionSerializer
from plombr.tables.registry import registry
from plombr.tables.table import ModelTable


class SearchFilter(CharFilter):
    def __init__(self, *args, fields, **kwargs):
        self.fields = fields
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        q = reduce(operator.or_, [Q(**{f + '__icontains': value}) for f in self.fields])
        return qs.filter(q)


class TableApiMixin:
    def check_table_permissions(self, request, table):
        for permission in table.get_permissions():
            if not permission.has_permission(request, self):
                self.permission_denied(
                    request, message=getattr(permission, 'message', None), code=getattr(permission, 'code', None)
                )

    def get_table(self, request, key):
        table_class = registry.get(key)
        table = table_class(request)
        self.check_table_permissions(request, table)
        return table


class ColumnFilter(BooleanFilter):
    def __init__(self, *args, table_filter, **kwargs):
        self.table_filter = table_filter
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        if value:
            return self.table_filter.apply(qs)
        return qs


class TableDataApiView(TableApiMixin, APIView):
    def get(self, request, *args, key, **kwargs):
        table = self.get_table(request, key)
        fs = self.get_filterset(table, request)
        return Response({'rows': table.serialize_rows(fs.qs)})

    def patch(self, request, *args, key, **kwargs):
        table = self.get_table(request, key)
        obj_id = request.query_params['objId']
        obj = table.get_queryset().get(pk=obj_id)
        for key, value in request.data.items():
            if key in table.columns and hasattr(obj, key):
                setattr(obj, key, value)
        obj.save()
        return Response({})

    def get_filterset_class(self, table: Table) -> Optional[Type[filters.FilterSet]]:
        if not isinstance(table, ModelTable):
            return None

        columns = table.columns.values()
        sortable_fields = tuple(x.name for x in columns if x.sortable)
        searchable_fields = tuple(x.name for x in columns if x.searchable)
        column_filters = {}
        for col in columns:
            if not col.filters:
                continue
            for f in col.filters:
                column_filters[f'{col.name}__{f.name}'] = ColumnFilter(table_filter=f)

        class FilterSetMeta:
            model = table._meta.model
            fields = {f: ('icontains',) for f in searchable_fields}

        filterset_class = type(
            "TableFilterSet",
            (filters.FilterSet,),
            {
                'o': OrderingFilter(fields=sortable_fields),
                'q': SearchFilter(fields=searchable_fields),
                'Meta': FilterSetMeta,
                **column_filters,
            },
        )
        return cast(Type[filters.FilterSet], filterset_class)

    def get_filterset(self, table: Table, request):
        filterset_class = self.get_filterset_class(table)
        if filterset_class is not None:
            return filterset_class(request.GET, table.get_queryset())


class TableActionApiView(TableApiMixin, APIView):
    permission_classes = ()

    def post(self, request, *args, key, **kwargs):
        table = self.get_table(request, key)
        request_serializer = ApplyTableActionSerializer(data=request.data)
        request_serializer.is_valid(raise_exception=True)
        request_data = request_serializer.validated_data
        ids = request_data['ids']
        qs = table.get_queryset().filter(id__in=ids)
        action = table.get_action_by_name(request_data['action'])
        action.apply(qs)
        return Response(
            {
                'message': {
                    'success': action.get_success_message(),
                }
            }
        )
