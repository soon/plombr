from rest_framework import serializers
from rest_framework.fields import BooleanField, CharField

from plombr.components.serializers import ComponentSerializer
from plombr.utils.serializers import CamelCaseSerializer


class ApplyTableActionSerializer(serializers.Serializer):
    action = serializers.CharField()
    ids = serializers.ListField()


class TableWidgetSerializer(CamelCaseSerializer):
    kind = CharField()


class TableFilterSerializer(CamelCaseSerializer):
    name = CharField()
    label = CharField()


class TableColumnSerializer(CamelCaseSerializer):
    type = CharField()
    name = CharField()
    label = CharField(source='get_label')
    sortable = BooleanField()
    filters = TableFilterSerializer(many=True)
    searchable = BooleanField()
    widget = TableWidgetSerializer()


class TableActionSerializer(CamelCaseSerializer):
    name = CharField()
    label = CharField()


class TableSerializer(ComponentSerializer):
    columns = TableColumnSerializer(many=True, source='columns_list')
    data_url = CharField(source='_meta.data_url')
    action_url = CharField(source='_meta.action_url')
    add_new_url = CharField(source='_meta.add_new_url')
    actions = TableActionSerializer(many=True, source='_meta.actions')


class RowActionSerializer(CamelCaseSerializer):
    kind = CharField()
    text = CharField()
    table_action_key = CharField()


class LinkActionSerializer(CamelCaseSerializer):
    kind = CharField()
    text = CharField()
    url = CharField()
