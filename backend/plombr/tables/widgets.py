class BaseTableWidget:
    kind = None


class IncrementDecrementIntegerWidget(BaseTableWidget):
    kind = 'decrement-increment-integer'


class ImageWidget(BaseTableWidget):
    kind = 'image'
