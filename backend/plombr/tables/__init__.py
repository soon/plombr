from django.utils.module_loading import autodiscover_modules

from plombr.tables.table import Column, Table, TableAction, TableColumnFilter


__all__ = [
    'Table',
    'Column',
    'TableColumnFilter',
    'TableAction',
    'autodiscover',
]


def autodiscover():
    autodiscover_modules('tables')
