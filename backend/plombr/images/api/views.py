from django.core.files.storage import default_storage
from rest_framework.response import Response
from rest_framework.views import APIView


class UploadImageApiView(APIView):
    def post(self, request, *args, **kwargs):
        file = request.FILES['file']
        storage = default_storage
        filename = storage.generate_filename(file.name)
        final_filename = storage.save(filename, file.file)
        return Response({'name': final_filename, 'url': storage.url(final_filename)})
