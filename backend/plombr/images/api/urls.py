from django.urls import path

from plombr.images.api import views


urlpatterns = [path('upload', views.UploadImageApiView.as_view(), name='upload')]
