# noinspection PyUnresolvedReferences
from plombr.settings.base import *


INSTALLED_APPS += [
    'plombr.tables.api.tests',
    'plombr.forms.tests',
    'plombr.forms_extensions.tests',
]
