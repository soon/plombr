from django.urls import path

from plombr.menu.api import views


app_name = 'menu'

urlpatterns = [
    path('items', views.MenuApiView.as_view()),
]
