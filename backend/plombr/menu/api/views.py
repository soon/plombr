from rest_framework.generics import RetrieveAPIView

from plombr.menu.api import serializers
from plombr.menu.provider import get_menu_items
from plombr.menu.types import Menu


class MenuApiView(RetrieveAPIView):
    serializer_class = serializers.MenuSerializer

    def get_object(self):
        return Menu(items=get_menu_items())
