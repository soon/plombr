from rest_framework.fields import CharField, ListField
from rest_framework.serializers import Serializer


class RecursiveField(Serializer):
    def to_representation(self, value):
        serializer = self.parent.parent.__class__(value, context=self.context)
        return serializer.data


class IconField(CharField):
    def to_representation(self, instance):
        instance = instance.value
        return super().to_representation(instance)


class MenuItemSerializer(Serializer):
    title = CharField()
    url = CharField(
        required=False,
    )
    action = CharField(
        required=False,
    )
    items = RecursiveField(
        many=True,
        required=False,
    )
    icon = IconField(
        required=False,
    )


class MenuSerializer(Serializer):
    items = ListField(child=MenuItemSerializer())
