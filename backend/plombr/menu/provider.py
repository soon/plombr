from importlib import import_module

from django.conf import settings


def get_menu_items():
    module = import_module(settings.MENU_CONFIG_MODULE)
    return list(module.menu_items)
