from collections import namedtuple
from enum import Enum
from typing import Optional, Union


Menu = namedtuple('Menu', ('items',))


class MenuItem:
    def __init__(self, title: str, url: str, icon: Optional[Union[str, Enum]] = None):
        self.title = title
        self.url = url
        self.icon = icon


class ActionMenuItem:
    def __init__(self, title: str, action: str, icon: Optional[Union[str, Enum]] = None):
        self.title = title
        self.action = action
        self.icon = icon


class SubMenuItem:
    def __init__(self, title: str, items, icon: Optional[Union[str, Enum]] = None):
        self.title = title
        self.items = items
        self.icon = icon
