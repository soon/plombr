from enum import Enum


class KnownIcons(Enum):
    TABLE_OUTLINED = 'table-outlined'
    BAR_CHART_OUTLINED = 'bar-chart-outlined'
    DEPLOYMENT_UNIT_OUTLINED = 'deployment-unit-outlined'
    CONTAINER_OUTLINED = 'container-outlined'
    VIDEO_CAMERA_OUTLINED = 'video-camera-outlined'
    SHOPPING_CART_OUTLINED = 'shopping-cart-outlined'
    KEY_OUTLINED = 'key-outlined'
    CONTACTS_OUTLINED = 'contacts-outlined'
    LOGOUT_OUTLINED = 'logout-outlined'
