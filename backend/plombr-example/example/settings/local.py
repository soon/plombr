import os

os.environ['DEBUG'] = 'true'

# noinspection PyUnresolvedReferences
from example.settings.base import *

del os.environ['DEBUG']
