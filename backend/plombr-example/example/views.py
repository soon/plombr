from rest_framework.views import exception_handler as drf_exception_handler

from plombr.actions.api.serializers import ActionSerializer


def custom_exception_handler(exc, context):
    response = drf_exception_handler(exc, context)
    get_actions = getattr(exc, 'get_actions', None)
    if callable(get_actions):
        response.data['_actions'] = ActionSerializer(many=True).to_representation(get_actions())

    return response
