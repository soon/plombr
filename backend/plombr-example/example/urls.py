from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from rest_framework_simplejwt.views import TokenRefreshView

import example.app.api.urls
import plombr.forms.api.urls
import plombr.images.api.urls
import plombr.menu.api.urls
import plombr.pages.api.urls
import plombr.tables.api.urls
from example.auth.views import TokenObtainPairView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(example.app.api.urls)),
    path('api/v1/tables/', include(plombr.tables.api.urls)),
    path('api/v1/pages/', include(plombr.pages.api.urls)),
    path('api/v1/images/', include(plombr.images.api.urls)),
    path('api/v1/forms/', include(plombr.forms.api.urls)),
    path('api/v1/menu/', include(plombr.menu.api.urls)),
    path('api/v1/auth/token', TokenObtainPairView.as_view()),
    path('api/v1/auth/token/refresh', TokenRefreshView.as_view()),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
