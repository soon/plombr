from example.movies.forms import MovieFormLayout
from example.movies.tables import MoviesTable
from plombr.pages.page import FormLayoutPage, TablePage


class MoviesTablePage(TablePage):
    title = 'Movies'
    table_class = MoviesTable


class MoviesFormPage(FormLayoutPage):
    title = 'Add Movie'
    form_layout_class = MovieFormLayout
