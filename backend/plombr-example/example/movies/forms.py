from django.forms import ModelForm
from django.urls import reverse_lazy

from example.movies import models
from plombr.forms.layout import FormLayout


class MovieForm(ModelForm):
    class Meta:
        model = models.Movie
        fields = (
            'title',
            'poster',
        )


class MovieFormLayout(FormLayout):
    class Meta:
        form_class = MovieForm
        submit_url = reverse_lazy('movies-form')
