from django.urls import path

from example.movies import pages


urlpatterns = [
    path('', pages.MoviesTablePage, name='movies'),
    path('/new', pages.MoviesFormPage, name='new-movie'),
]
