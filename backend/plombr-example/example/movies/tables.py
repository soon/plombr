from example.movies import models
from plombr.pages.urls import reverse_page_lazy
from plombr.tables.table import Column, ModelTable


class MoviesTable(ModelTable):
    poster = Column(
        type='image',
        searchable=False,
    )

    class Meta:
        model = models.Movie
        fields = (
            'id',
            'title',
            'poster',
        )
        add_new_url = reverse_page_lazy('new-movie')

    @classmethod
    def prepare_poster(cls, record):
        return record.poster.url
