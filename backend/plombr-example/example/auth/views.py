from rest_framework.exceptions import AuthenticationFailed
from rest_framework_simplejwt.views import TokenObtainPairView as DrfTokenObtainPairView

from plombr.actions.types import Action


class CustomAuthenticationFailed(AuthenticationFailed):
    def get_actions(self):
        return [
            Action(
                'showNotification',
                {
                    'message': 'Invalid username or password',
                    'duration': 10,
                    'type': 'error',
                },
            ),
        ]


class TokenObtainPairView(DrfTokenObtainPairView):
    def post(self, request, *args, **kwargs):
        try:
            return super().post(request, *args, **kwargs)
        except AuthenticationFailed as exc:
            raise CustomAuthenticationFailed(detail=exc.detail, code=exc.status_code)
