from django.urls import path

from example.users.api.views import GenerateUserApiView


urlpatterns = [
    path('generate', GenerateUserApiView.as_view(), name='generate'),
]
