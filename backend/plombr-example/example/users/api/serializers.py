from plombr.actions.api.serializers import ActionSerializer
from plombr.utils.serializers import CamelCaseSerializer


class GenerateUserResponseSerializer(CamelCaseSerializer):
    _actions = ActionSerializer(many=True)
