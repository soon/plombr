from django.contrib.auth import get_user_model
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken

from example.app.models import TodoTask
from example.users.api.serializers import GenerateUserResponseSerializer
from example.users.generator import generate_password, generate_username
from plombr.actions.types import Action


User = get_user_model()


class GenerateUserApiView(APIView):
    @classmethod
    def generate_objects(cls, user):
        TodoTask.objects.create(
            title='Explore platform',
            description='Take your time to click on different buttons',
            priority=1,
            user=user,
        )

    def post(self, request, *args, **kwargs):
        username = generate_username()
        password = generate_password()
        user = User.objects.create_user(username, password)
        self.generate_objects(user)
        token = RefreshToken.for_user(user)
        response_serializer = GenerateUserResponseSerializer(
            {
                '_actions': [
                    Action(
                        'setToken',
                        {
                            'access': str(token.access_token),
                            'refresh': str(token),
                        },
                    ),
                    Action('routeTo', '/'),
                    Action(
                        'showNotification',
                        {
                            'message': "You've just logged in",
                            'description': f'Username: {username}, password: {password}',
                            'duration': 10,
                            'type': 'success',
                        },
                    ),
                ]
            }
        )
        return Response(response_serializer.data)
