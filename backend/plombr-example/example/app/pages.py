from typing import List

from django.urls import reverse_lazy

from example.app.forms import TodoTaskFormLayout
from example.app.tables import TasksTable
from plombr.components.base import Component
from plombr.pages.components import Col, PieChart, Row
from plombr.pages.page import FormLayoutPage, GenericPage, TablePage


class TasksTablePage(TablePage):
    title = 'Tasks'
    table_class = TasksTable


class TasksFormLayoutPage(FormLayoutPage):
    form_layout_class = TodoTaskFormLayout

    def get_id(self):
        if self.kwargs:
            return self.kwargs.get('id')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        obj_id = self.get_id()
        if obj_id is not None:
            kwargs['object_id'] = obj_id
        return kwargs

    def get_title(self):
        obj_id = self.get_id()
        if obj_id is not None:
            return f'Edit Task #{obj_id}'
        return 'New Task'


class TasksChartComponent(PieChart):
    data_url = reverse_lazy('tasks-chart')


class TasksChartPage(GenericPage):
    title = 'Tasks Charts'

    def get_items(self) -> List[Component]:
        return [TasksChartComponent()]


class TasksCompoundPage(GenericPage):
    title = 'Compound Tasks Page'

    def get_items(self):
        return [
            Col(
                Row(Col(TodoTaskFormLayout(request=self.request)), Col(TasksChartComponent())),
                Row(Col(TasksTable(request=self.request))),
            )
        ]
