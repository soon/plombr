from datetime import timedelta

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.utils import timezone


User = get_user_model()


class Command(BaseCommand):
    def handle(self, *args, **options):
        users = User.objects.exclude(is_staff=True).exclude(is_superuser=True).filter(
            is_active=True,
            date_joined__lte=timezone.now() - timedelta(hours=2),
        )
        self.stdout.write(f'Deactivating {users.count()} users')
        users.update(
            is_active=False,
        )
