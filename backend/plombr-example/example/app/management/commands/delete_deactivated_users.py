from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand


User = get_user_model()


class Command(BaseCommand):
    def handle(self, *args, **options):
        users = User.objects.exclude(is_staff=True).exclude(is_superuser=True).filter(is_active=False)
        self.stdout.write(f'Deleting {users.count()} users')
        users.delete()
