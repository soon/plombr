from django.utils import timezone
from django.utils.translation import ngettext
from rest_framework.permissions import IsAuthenticated

from example.app import models
from plombr.pages.urls import reverse_page, reverse_page_lazy
from plombr.tables.actions import Link, RowAction
from plombr.tables.table import ActionsColumn, Column, DeleteColumn, ModelTable, Table, TableAction, TableColumnFilter
from plombr.tables.widgets import IncrementDecrementIntegerWidget


class SampleTable(Table):
    date = Column()
    amount = Column()
    type = Column()
    note = Column()
    action = Column()


class MarkAsCompletedAction(TableAction):
    name = 'mark_as_completed'
    label = 'Mark As Completed'
    success_message = 'Task completed.'

    def apply(self, qs):
        for obj in qs:
            obj.completed_at = timezone.now()
            obj.save()
        self.count = qs.count()

    def get_success_message(self):
        if self.count == 0:
            return 'No tasks changed.'
        if self.count == 1:
            return 'Task completed!'
        return f'{self.count} tasks completed!'


class MarkAsNotCompletedAction(TableAction):
    name = 'mark_as_not_completed'
    label = 'Mark As Not Completed'
    success_message = 'Task reopened.'

    def apply(self, qs):
        for obj in qs:
            obj.completed_at = None
            obj.save()
        self.count = qs.count()

    def get_success_message(self):
        if self.count == 0:
            return 'No tasks changed.'
        if self.count == 1:
            return 'Task reopened!'
        return f'{self.count} tasks reopened!'


class OpenTasksColumnFilter(TableColumnFilter):
    name = 'uncompleted'
    label = 'Open Tasks Only'

    def apply(self, qs):
        return qs.filter(completed_at=None)


class CompletedTasksColumnFilter(TableColumnFilter):
    name = 'completed'
    label = 'Completed Tasks Only'

    def apply(self, qs):
        return qs.exclude(completed_at=None)


class TasksTable(ModelTable):
    priority = Column(widget=IncrementDecrementIntegerWidget())
    completed_at = Column(
        filters=[
            OpenTasksColumnFilter(),
            CompletedTasksColumnFilter(),
        ]
    )
    actions = ActionsColumn(label='Actions', sortable=False)
    delete_task = DeleteColumn(label='', sortable=False)

    class Meta:
        model = models.TodoTask
        fields = (
            'id',
            'priority',
            'title',
            'description',
            'completed_at',
        )
        add_new_url = reverse_page_lazy('new-task')
        actions = [
            MarkAsCompletedAction,
            MarkAsNotCompletedAction,
        ]
        permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(
                user=self.request.user,
            )
            .order_by('-created')
        )

    @classmethod
    def prepare_actions(cls, record):
        actions = [
            Link(
                text='Edit',
                url=reverse_page('edit-task', args=(record.pk,)),
            )
        ]
        if record.completed_at:
            actions.append(RowAction(text='Reopen', table_action_key='mark_as_not_completed'))
        else:
            actions.append(RowAction(text='Close', table_action_key='mark_as_completed'))
        return [x.serialize() for x in actions]

    @classmethod
    def prepare_edit_task(cls, record):
        return {'url': reverse_page('edit-task', args=(record.pk,)), 'text': 'Edit'}
