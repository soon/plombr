import stringcase
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from example.app import models
from example.movies.models import Movie
from plombr.pages.urls import reverse_page


def to_camel_case(obj):
    if isinstance(obj, dict):
        return {stringcase.camelcase(k): to_camel_case(v) for k, v in obj.items()}
    if isinstance(obj, list):
        return list(map(to_camel_case, obj))
    return obj


class CamelCaseJsonResponse(Response):
    def __init__(self, data, *args, **kwargs):
        data = to_camel_case(data)
        super().__init__(data, *args, **kwargs)


class TaskFormApiView(APIView):
    def post(self, request, *args, **kwargs):
        data = request.data
        if 'id' in data:
            task = models.TodoTask.objects.get(pk=data['id'])
            fields = {
                'title',
                'description',
                'completed_at',
                'priority',
            }
            for k, v in data.items():
                if k in fields:
                    setattr(task, k, v)
            task.save()
            message = {'success': 'Task changed!'}
        else:
            models.TodoTask.objects.create(
                title=data['title'],
                description=data.get('description') or '',
                completed_at=data.get('completed_at'),
                priority=data.get('priority') or 0,
            )
            message = {'success': 'Task added!'}
        return CamelCaseJsonResponse({'action': {'type': 'redirect', 'url': '/tasks'}, 'message': message})


class MovieFormApiView(APIView):
    def post(self, request, *args, **kwargs):
        data = request.data
        Movie.objects.create(
            title=data['title'],
            poster=data['poster'],
        )
        return CamelCaseJsonResponse(
            {
                'action': {'type': 'redirect', 'url': reverse_page('movies')},
                'message': 'Movie added!',
            }
        )


class TaskChartApiView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        qs = models.TodoTask.objects.filter(user=self.request.user)
        return CamelCaseJsonResponse(
            {
                'title': 'Tasks Distribution',
                'source': [
                    ['Completed', qs.exclude(completed_at=None).count()],
                    ['Open', qs.filter(completed_at=None).count()],
                ],
            }
        )
