from django.urls import include, path

import example.users.api.urls
from example.app.api import views


urlpatterns = [
    path('task-form', views.TaskFormApiView.as_view(), name='task-form'),
    path('tasks-chart', views.TaskChartApiView.as_view(), name='tasks-chart'),
    path('movies-form', views.MovieFormApiView.as_view(), name='movies-form'),
    path('users/', include(example.users.api.urls)),
]
