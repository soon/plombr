import operator
from functools import reduce

from django.db.models import Q
from django_filters import BooleanFilter, CharFilter, OrderingFilter, rest_framework as filters

from example.app.models import TodoTask


class SearchFilter(CharFilter):
    def __init__(self, *args, fields, **kwargs):
        self.fields = fields
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        q = reduce(operator.or_, [Q(**{f + '__icontains': value}) for f in self.fields])
        return qs.filter(q)


class CompletedFilter(BooleanFilter):
    def filter(self, qs, value):
        if value:
            qs = qs.exclude(completed_at=None)
        return qs


class UncompletedFilter(BooleanFilter):
    def filter(self, qs, value):
        if value:
            qs = qs.filter(completed_at=None)
        return qs


class TodoTaskFilter(filters.FilterSet):
    o = OrderingFilter(
        fields=(
            'title',
            'description',
            'completed_at',
        )
    )
    q = SearchFilter(
        fields=(
            'title',
            'description',
            'completed_at',
        )
    )
    completed_at__completed = CompletedFilter()
    completed_at__uncompleted = UncompletedFilter()

    class Meta:
        model = TodoTask
        fields = {
            'title': ('icontains',),
            'description': ('icontains',),
            'completed_at': ('icontains',),
        }
