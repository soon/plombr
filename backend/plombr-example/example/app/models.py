from django.contrib.auth import get_user_model
from django.db import models
from model_utils.models import SoftDeletableModel, TimeStampedModel


User = get_user_model()


class TodoTask(TimeStampedModel, SoftDeletableModel):
    title = models.CharField(
        max_length=200,
    )
    description = models.TextField(
        blank=True,
    )
    priority = models.PositiveIntegerField(
        default=0,
    )
    completed_at = models.DateTimeField(
        null=True,
        blank=True,
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.title
