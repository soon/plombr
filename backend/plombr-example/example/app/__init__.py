from django.apps import AppConfig


class Config(AppConfig):
    name = 'example.app'
    label = 'app'


default_app_config = 'example.app.Config'
