from django.contrib import admin
from django.contrib.admin import ModelAdmin

from example.app import models


@admin.register(models.TodoTask)
class TodoTaskAdmin(ModelAdmin):
    pass
