from django.urls import path

from example.app import pages


urlpatterns = [
    path('tasks', pages.TasksTablePage, name='tasks'),
    path('tasks/new', pages.TasksFormLayoutPage, name='new-task'),
    path('tasks/<int:id>', pages.TasksFormLayoutPage, name='edit-task'),
    path('tasks/chart', pages.TasksChartPage, name='tasks-chart'),
    path('tasks/compound', pages.TasksCompoundPage, name='tasks-compound'),
]
