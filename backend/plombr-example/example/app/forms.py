from django import forms

from example.app import models
from plombr.forms.fields import TextAreaField
from plombr.forms.layout import FormLayout
from plombr.pages.urls import reverse_page_lazy


class TaskForm(forms.ModelForm):
    class Meta:
        model = models.TodoTask
        fields = (
            'title',
            'description',
            'priority',
            'completed_at',
        )

    def __init__(self, *args, request, **kwargs):
        super().__init__(*args, **kwargs)
        self.request = request

    def save(self, commit=True):
        self.instance.user = self.request.user
        return super().save(commit=commit)


class TodoTaskFormLayout(FormLayout):
    description = TextAreaField()

    class Meta:
        form_class = TaskForm

    def get_success_response(self):
        return {
            'message': {
                'success': 'Task added!',
            },
            'action': {'type': 'redirect', 'url': reverse_page_lazy('tasks')},
        }

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs
