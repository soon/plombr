from typing import List

from plombr.components.base import Component
from plombr.components.common import Text
from plombr.components.modals.types import Modal, OpenModalButton
from plombr.pages.components import Row
from plombr.pages.page import GenericPage


class PlaygroundPage(GenericPage):
    title = 'Playground'

    def get_items(self) -> List[Component]:
        modal = Modal(title='Example modal', body='Modal text', key='example-modal')
        return [
            Row(
                Text(data='A playground for some experimental components.'),
            ),
            Row(
                OpenModalButton(
                    title='Open modal',
                    modal=modal,
                ),
            ),
            modal,
        ]
