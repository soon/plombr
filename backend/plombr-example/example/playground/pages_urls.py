from django.urls import path

from example.playground import pages


app_name = 'playground'

urlpatterns = [
    path('test', pages.PlaygroundPage, name='index'),
]
