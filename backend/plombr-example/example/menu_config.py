from plombr.icons.enums import KnownIcons
from plombr.menu.types import ActionMenuItem, MenuItem, SubMenuItem
from plombr.pages.urls import reverse_page


menu_items = [
    SubMenuItem(
        title='Tasks',
        icon=KnownIcons.CONTAINER_OUTLINED,
        items=[
            MenuItem(
                title='Table',
                url=reverse_page('tasks'),
                icon=KnownIcons.TABLE_OUTLINED,
            ),
            MenuItem(
                title='Charts',
                url=reverse_page('tasks-chart'),
                icon=KnownIcons.BAR_CHART_OUTLINED,
            ),
            MenuItem(
                title='Compound',
                url=reverse_page('tasks-compound'),
                icon=KnownIcons.DEPLOYMENT_UNIT_OUTLINED,
            ),
        ],
    ),
    MenuItem(
        title='Playground',
        url=reverse_page('playground:index'),
        icon=KnownIcons.TABLE_OUTLINED,
    ),
    ActionMenuItem(
        title='Logout',
        action='logout',
        icon=KnownIcons.LOGOUT_OUTLINED,
    ),
]
