from django.urls import include, path

import example.app.pages_urls
import example.playground.pages_urls


urlpatterns = [
    path('', include(example.app.pages_urls)),
    path('playground/', include(example.playground.pages_urls)),
]
